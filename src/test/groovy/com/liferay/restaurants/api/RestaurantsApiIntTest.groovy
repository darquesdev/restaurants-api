package com.liferay.restaurants.api

import com.liferay.restaurants.RestaurantsApp
import com.liferay.restaurants.api.model.RestaurantDto
import com.liferay.restaurants.api.model.RestaurantEditableDto
import com.liferay.restaurants.api.model.search.SearchResponseResource
import com.liferay.restaurants.application.RestaurantsApplicationService
import com.liferay.restaurants.model.domain.restaurant.Category
import com.liferay.restaurants.model.domain.restaurant.Location
import com.liferay.restaurants.model.domain.restaurant.Restaurant
import com.liferay.restaurants.model.domain.restaurant.RestaurantIdGenerator
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.test.web.client.TestRestTemplate
import org.springframework.data.elasticsearch.core.ElasticsearchTemplate
import org.springframework.hateoas.PagedResources
import org.springframework.http.HttpMethod
import org.springframework.test.context.ActiveProfiles
import spock.lang.Shared
import spock.lang.Specification
import spock.lang.Unroll

import static org.springframework.boot.test.context.SpringBootTest.WebEnvironment.RANDOM_PORT

/**
 * @author david.arques
 */
@SpringBootTest(classes = RestaurantsApp, webEnvironment= RANDOM_PORT)
@ActiveProfiles("test")
class RestaurantsApiIntTest extends Specification {

    private Restaurant validRestaurant1
    private Restaurant validRestaurant2
    private Restaurant validRestaurant3
    private Restaurant validRestaurant4
    private Restaurant VALID_RESTAURANT_5

    @Autowired
    private RestaurantsApplicationService restaurantsApplicationService

    @Autowired
    private ElasticsearchTemplate elasticsearchTemplate;

    @Autowired
    private TestRestTemplate restTemplate

    @Autowired
    private RestaurantIdGenerator restaurantIdGenerator

    def setup(){

        elasticsearchTemplate.deleteIndex(Restaurant.INDEX_NAME)
        elasticsearchTemplate.createIndex(Restaurant.INDEX_NAME)
        elasticsearchTemplate.putMapping(Restaurant)
        elasticsearchTemplate.refresh(Restaurant)

        validRestaurant1 = createValidRestaurant1()
        validRestaurant2 = createValidRestaurant2()
        validRestaurant3 = createValidRestaurant3()
        validRestaurant4 = createValidRestaurant4()

        restaurantsApplicationService.createRestaurant(RestaurantEditableDto.fromModel(validRestaurant1))
        restaurantsApplicationService.createRestaurant(RestaurantEditableDto.fromModel(validRestaurant2))
        restaurantsApplicationService.createRestaurant(RestaurantEditableDto.fromModel(validRestaurant3))
        restaurantsApplicationService.createRestaurant(RestaurantEditableDto.fromModel(validRestaurant4))
    }

    @Unroll
    def "The list of restaurants has support for pagination"(int page, int size, PagedResources.PageMetadata pagination) {


        def actualPagination = restTemplate.exchange("/api/restaurants/search?page={page}&size={size}",
                HttpMethod.GET, null, SearchResponseResource, page, size).getBody().getData().getMetadata();

        expect:
        actualPagination == pagination

        where:
        page | size | pagination
           0 |    2 | new PagedResources.PageMetadata(2, 0, 4, 2)
           1 |    2 | new PagedResources.PageMetadata(2, 1, 4, 2)
           0 |    4 | new PagedResources.PageMetadata(4, 0, 4, 1)
    }

    @Unroll
    def "The list of restaurants has support for sorting by certain fields"(String sort, String dir, String restaurant) {

        expect:

        RestaurantDto actualRestaurant = restTemplate.exchange("/api/restaurants/search?sort={sort},{dir}", HttpMethod.GET, null,
                SearchResponseResource, sort, dir).getBody().getData().getContent().stream().findFirst().get().getContent()

        println actualRestaurant

        actualRestaurant.getName() == restaurant

        where:
         sort               |  dir  | restaurant
        "price"             | "asc" | "La Parranda"
        "price.value"       | "asc" | "La Parranda"
        "price"             | "desc"| "Yakitoro by Chicote"
        "price.value"       | "desc"| "Yakitoro by Chicote"
        "rating"            | "asc" | "La Parranda"
        "rating.value"      | "asc" | "La Parranda"
        "rating.reviewCount"| "asc" | "Yakitoro by Chicote"
        "rating.reviewCount"| "desc"| "Best Pizza"
    }

    @Unroll
    def "The list of restaurants has support for filtering by certain fields"(String filter, String value, int totalElements) {

        expect:
        def actualTotalElements = restTemplate.exchange("/api/restaurants/search?{filter}={value}", HttpMethod.GET, null,
                SearchResponseResource, filter, value).getBody().getData().getMetadata().totalElements
        actualTotalElements == totalElements

        where:
        filter       | value                   | totalElements
        "categories" | "indian"                | 0
        "categories" | "japanese"              | 2
        "categories" | "japanese,tapas"        | 3
        "categories" | "japanese,tapas,pizza"  | 4
        "price"      | "1"                     | 1
        "price"      | "2"                     | 1
        "price"      | "1,2"                   | 2
        "price"      | "1,2,3,4"               | 4
        "countries"  | "ES"                    | 3
        "countries"  | "US"                    | 1
        "countries"  | "ES,US"                 | 4
        "countries"  | "UK"                    | 0
        "cities"     | "Madrid"                | 2
        "cities"     | "Brooklyn"              | 1
        "cities"     | "Murcia"                | 1
        "cities"     | "Barcelona"             | 0
        "cities"     | "Madrid,Murcia,Brooklyn"| 4
        "states"     | "NY"                    | 1
        "states"     | "CA"                    | 0
        "states"     | "NY,M,MU"               | 4
    }


    @Unroll
    def "The list of restaurants has support for searching by geolocation"(double latitude, double longitude, int radius, int total) {

        expect:
        def actualTotalElements = restTemplate.exchange("/api/restaurants/search?latitude={latitude}&longitude={longitude}&radius={radius}",
                HttpMethod.GET, null, SearchResponseResource, latitude, longitude, radius).getBody().getData().getMetadata().totalElements
        actualTotalElements == total

        where:
        latitude   | longitude  | radius | total
        40.411446D | -3.698794D | 1      | 1    // YOKALOKA (Lavapies, Madrid)
        40.411446D | -3.698794D | 1000   | 2    // YOKALOKA & Yakitoro (Gran Via)
        40.411446D | -3.698794D | 350000 | 3    // YOKALOKA & Yakitoro & La Parranda (Murcia)
        40.411446D | -3.698794D | 6000000| 4    // YOKALOKA & Yakitoro & La Parranda & Best Pizza (Brooklyn)
    }

    @Unroll
    def "The list of restaurants has support for fulltext searching by multiple fields"(String term, int total) {

        expect:
        def actualTotalElements = restTemplate.exchange("/api/restaurants/search?term={term}",
                HttpMethod.GET, null, SearchResponseResource, term).getBody().getData().getMetadata().totalElements
        actualTotalElements == total

        where:
        term               | total
        "Chicote"          | 1      //name
        "amantes del mundo"| 1      //description
        "Takeaway"         | 1      //categories.displayName
        "Madrid"           | 2      //location.city
        "Havemeyer St"     | 1      //location.address1, location.address1
    }

    @Unroll
    def "The list of restaurants has support for aggregations by multiple fields"(String term, String value, long total) {

        expect:
        def actualTotalElements = restTemplate.exchange("/api/restaurants",
                HttpMethod.GET, null, SearchResponseResource, term).getBody().getAggregations().get(term).get(value)
        actualTotalElements == total

        where:
        term         | value            | total
        "categories" | "japanese"       | 2
        "categories" | "mediterranean"  | 1
        "categories" | "traditional"    | 1
        "categories" | "pizza"          | 1
        "price"      | "1"              | 1
        "price"      | "2"              | 1
        "price"      | "3"              | 1
        "price"      | "4"              | 1
        "rating"     | "3"              | 2
        "rating"     | "5"              | 2
        "location"   | "Madrid"         | 2
        "location"   | "Brooklyn"       | 1
        "location"   | "Murcia"         | 1
        "location.country" | "ES"       | 3
        "location.country" | "US"       | 1
    }

    private Restaurant createValidRestaurant1() {//NOSONAR
        Restaurant.builder(restaurantIdGenerator.generateId())
                .withName("YOKALOKA")
                .withDescription("Punto de encuentro de los amantes del mundo gastron\u00F3mico y cultural japon\u00E9s")
                .withCategories(new Category("japanese", "Japanese"), new Category("sushi", "Sushi Bars"))
                .withLocation(Location.builder()
                .withAddress1("Calle Santa Isabel, 5, Planta Baja")
                .withAddress2("Mercado de Ant\u00F3n Mart\u00EDn")
                .withCity("Madrid")
                .withPostalCode("28012")
                .withCountry("ES")
                .withState("M")
                .build())
                .withCoordinates(40.411446D, -3.698794D)
                .withPhone("610 60 27 22", "ES")
                .withPrice(3)
                .withRating(4.5D, 325)
                .createdAt(new Date())
                .modifiedAt(new Date())
                .build()
    }
    private Restaurant createValidRestaurant2() {//NOSONAR
        Restaurant.builder(restaurantIdGenerator.generateId())
                .withName("Yakitoro by Chicote")
                .withDescription("Yakitoro es un espacio inspirado en la tradicional taberna japonesa de yakitori. Le damos el toque espa\u00F1ol y lo interpretamos a nuestra manera, para que todo sea muy cercano y muy nuestro.")
                .withCategories(new Category("japanese", "Japanese"), new Category("signature_cuisine", "Signature cuisine"), new Category("asianfusion", "Asian Fusion"))
                .withLocation(Location.builder()
                .withAddress1("Calle Reina, 41")
                .withCity("Madrid")
                .withPostalCode("28004")
                .withCountry("ES")
                .withState("M")
                .build())
                .withCoordinates(40.4196848D, -3.6972273D)
                .withPhone("917 37 14 41", "ES")
                .withPrice(4)
                .withRating(4D, 95)
                .createdAt(new Date())
                .modifiedAt(new Date())
                .build()
    }
    private Restaurant createValidRestaurant3() {//NOSONAR
        Restaurant.builder(restaurantIdGenerator.generateId())
                .withName("La Parranda")
                .withDescription("Tapas, cocina murciana y arroces por encargo en un elegante espacio de aire rústico-chic con terraza cubierta.")
                .withCategories(new Category("tapas", "Tapas Bar"), new Category("tabern", "Tabern"), new Category("traditional", "Traditional"), new Category("mediterranean", "Mediterranean Food"))
                .withLocation(Location.builder()
                .withAddress1("Plaza San Juan, 7")
                .withAddress2("30003 Murcia, España")
                .withCity("Murcia")
                .withPostalCode("30003")
                .withCountry("ES")
                .withState("MU")
                .build())
                .withCoordinates(37.98287739999999D, -1.1263536D)
                .withPhone("968 220 675", "ES")
                .withPrice(1)
                .withRating(3.5D, 183)
                .createdAt(new Date())
                .modifiedAt(new Date())
                .build()
    }
    private Restaurant createValidRestaurant4() {//NOSONAR
        Restaurant.builder(restaurantIdGenerator.generateId())
                .withName("Best Pizza")
                .withDescription("The best pizza in Brooklyn")
                .withCategories(new Category("pizza", "Pizza"), new Category("takeaway", "Takeaway"))
                .withLocation(Location.builder()
                .withAddress1("33 Havemeyer St")
                .withAddress2("Brooklyn, NY 11211, EE. UU.")
                .withCity("Brooklyn")
                .withPostalCode("11211")
                .withCountry("US")
                .withState("NY")
                .build())
                .withCoordinates(40.71701257989272D, -73.95213632010727D)
                .withPhone("+1 718-599-2210", "US")
                .withPrice(2)
                .withRating(4.4D, 506)
                .createdAt(new Date())
                .modifiedAt(new Date())
                .build()
    }

}
