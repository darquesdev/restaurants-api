package com.liferay.restaurants.api;

import com.liferay.restaurants.api.model.RestaurantEditableDto;
import com.liferay.restaurants.api.model.search.SearchRequest;
import com.liferay.restaurants.api.model.search.SearchResponse;
import com.liferay.restaurants.application.RestaurantsApplicationService;
import com.liferay.restaurants.infraestructure.services.GooglePhoneNumberParser;
import com.liferay.restaurants.infraestructure.services.IsoCountryCodeValidatorImpl;
import com.liferay.restaurants.infraestructure.services.RestaurantIdGeneratorImpl;
import com.liferay.restaurants.model.domain.restaurant.*;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.config.EnableSpringDataWebSupport;
import org.springframework.hateoas.MediaTypes;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Date;
import java.util.Optional;

import static java.util.Arrays.asList;
import static org.hamcrest.CoreMatchers.is;
import static org.hamcrest.collection.IsCollectionWithSize.hasSize;
import static org.mockito.BDDMockito.given;
import static org.mockito.Mockito.*;
import static org.mockito.internal.verification.VerificationModeFactory.times;
import static org.springframework.http.MediaType.APPLICATION_JSON;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;


/**
 * @author david.arques
 */
@RunWith(SpringRunner.class)
@WebMvcTest(RestaurantsRestController.class)
@EnableSpringDataWebSupport //pagination and sort
@ActiveProfiles("test")
public class RestaurantsApiCrudTest {

    @Autowired
    private MockMvc mvc;

    @MockBean
    private RestaurantsApplicationService applicationService ;

    private String VALID_RESTAURANT_ID;
    private String ANOTHER_RESTAURANT_ID;
    private Restaurant aValidRestaurant;
    private Restaurant anotherValidRestaurant;

    @BeforeClass
    public static void init(){
        DomainRegistry.replaceServiceImplementations(
                new GooglePhoneNumberParser(),
                new IsoCountryCodeValidatorImpl()
        );
    }

    @Before
    public void setUp() {

        this.VALID_RESTAURANT_ID = new RestaurantIdGeneratorImpl().generateId();
        this.ANOTHER_RESTAURANT_ID = new RestaurantIdGeneratorImpl().generateId();

        this.aValidRestaurant = Restaurant.builder(VALID_RESTAURANT_ID)
                .withName("YOKALOKA")
                .withDescription("Punto de encuentro de los amantes del mundo gastron\u00F3mico y cultural japon\u00E9s")
                .withCategories(new Category("japanese", "Japanese"), new Category("sushi", "Sushi Bars"))
                .withLocation(Location.builder()
                        .withAddress1("Calle Santa Isabel, 5, Planta Baja")
                        .withAddress2("Mercado de Ant\u00F3n Mart\u00EDn")
                        .withCity("Madrid")
                        .withPostalCode("28012")
                        .withCountry("ES")
                        .withState("M")
                        .build())
                .withCoordinates(40.411446D, -3.698794D)
                .withPhone("610 60 27 22", "ES")
                .withPrice(3)
                .withRating(4.5D, 325)
                .createdAt(new Date())
                .modifiedAt(new Date())
                .build();

        this.anotherValidRestaurant = Restaurant.builder(ANOTHER_RESTAURANT_ID)
                .withName("Yakitoro by Chicote")
                .withDescription("Yakitoro es un espacio inspirado en la tradicional taberna japonesa de yakitori. Le damos el toque espa\u00F1ol y lo interpretamos a nuestra manera, para que todo sea muy cercano y muy nuestro.")
                .withCategories(new Category("japanese", "Japanese"), new Category("signature_cuisine", "Signature cuisine"), new Category("asianfusion", "Asian Fusion"))
                .withLocation(Location.builder()
                        .withAddress1("Calle Reina, 41")
                        .withCity("Madrid")
                        .withPostalCode("28004")
                        .withCountry("ES")
                        .withState("M")
                        .build())
                .withCoordinates(40.4196848D, -3.6972273)
                .withPhone("917 37 14 41", "ES")
                .withPrice(2)
                .withRating(4D, 95)
                .createdAt(new Date())
                .modifiedAt(new Date())
                .build();
    }

    @Test
    public void createRestaurantWithValidContentShouldReturnOk() throws Exception {

        //GIVEN
        RestaurantEditableDto createRestaurantCommand = RestaurantEditableDto.fromModel(aValidRestaurant);
        given(this.applicationService.createRestaurant(createRestaurantCommand))
                .willReturn(Optional.of(aValidRestaurant));

        //WHEN
        this.mvc.perform(post("/api/restaurants")
                .contentType(APPLICATION_JSON)
                .content(Files.readAllBytes(Paths.get(getClass().getResource("/japanese-valid1.json").toURI()))))
                .andDo(print())
                .andExpect(status().isCreated())
                .andExpect(header().string("Location", "http://localhost/api/restaurants/" + VALID_RESTAURANT_ID));

        //THEN
        verify(this.applicationService, times(1)).createRestaurant(createRestaurantCommand);
        verifyNoMoreInteractions(this.applicationService);
    }

    @Test
    public void updateExistingRestaurantWithValidContentShouldReturnOk() throws Exception {

        //GIVEN
        RestaurantEditableDto updateRestaurantCommand = RestaurantEditableDto.fromModel(aValidRestaurant);
        given(this.applicationService.restaurantExists(VALID_RESTAURANT_ID))
                .willReturn(true);
        given(this.applicationService.updateRestaurant(VALID_RESTAURANT_ID, updateRestaurantCommand))
                .willReturn(Optional.of(aValidRestaurant));
        //WHEN
        this.mvc.perform(put("/api/restaurants/{id}", VALID_RESTAURANT_ID)
                .contentType(APPLICATION_JSON)
                .content(Files.readAllBytes(Paths.get(getClass().getResource("/japanese-valid1.json").toURI()))))
                .andDo(print())
                .andExpect(status().isOk());

        //THEN
        verify(this.applicationService, times(1)).restaurantExists(VALID_RESTAURANT_ID);
        verify(this.applicationService, times(1)).updateRestaurant(VALID_RESTAURANT_ID, updateRestaurantCommand);
        verifyNoMoreInteractions(this.applicationService);
    }

    @Test
    public void updateNonExistentRestauranShouldReturnNotFound() throws Exception {

        String NON_EXISTENT_RESTAURANT_ID = "NON_EXISTENT_RESTAURANT_ID";
        given(this.applicationService.restaurantExists(NON_EXISTENT_RESTAURANT_ID))
                .willReturn(false);

        this.mvc.perform(put("/api/restaurants/{id}", NON_EXISTENT_RESTAURANT_ID)
                .contentType(APPLICATION_JSON)
                .content(Files.readAllBytes(Paths.get(getClass().getResource("/japanese-valid1.json").toURI()))))
                .andDo(print())
                .andExpect(status().isNotFound());

        verify(this.applicationService, times(1)).restaurantExists(NON_EXISTENT_RESTAURANT_ID);
        verifyNoMoreInteractions(this.applicationService);
    }

    @Test
    public void deleteExistingRestaurantShouldReturnOk() throws Exception {

        doAnswer(i -> true).when(applicationService).restaurantExists(VALID_RESTAURANT_ID);
        doNothing().when(applicationService).deleteRestaurant(VALID_RESTAURANT_ID);

        this.mvc.perform(delete("/api/restaurants/{id}", VALID_RESTAURANT_ID))
                .andDo(print())
                .andExpect(status().isOk());

        verify(this.applicationService, times(1)).restaurantExists(VALID_RESTAURANT_ID);
        verify(this.applicationService, times(1)).deleteRestaurant(VALID_RESTAURANT_ID);
        verifyNoMoreInteractions(this.applicationService);
    }

    @Test
    public void deleteNonExistentRestaurantShouldReturnNotFound() throws Exception {

        String NON_EXISTENT_RESTAURANT_ID = "NON_EXISTENT_RESTAURANT_ID";
        doAnswer(i -> false).when(applicationService).restaurantExists(NON_EXISTENT_RESTAURANT_ID);

        this.mvc.perform(delete("/api/restaurants/{id}", NON_EXISTENT_RESTAURANT_ID))
                .andExpect(status().isNotFound());

        verify(this.applicationService, times(1)).restaurantExists(NON_EXISTENT_RESTAURANT_ID);
        verifyNoMoreInteractions(this.applicationService);
    }

    @Test
    public void createRestaurantWithEmptyLocationShouldReturnBadRequest() throws Exception {

        // Location mandatory
        this.mvc.perform(post("/api/restaurants")
                .contentType(APPLICATION_JSON)
                .content(Files.readAllBytes(Paths.get(getClass().getResource("/japanese-invalid-noLocation.json").toURI()))))
                .andDo(print())
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.status", is(400)))
                .andExpect(jsonPath("$.message", is("validation error")));
    }

    @Test
    public void createRestaurantWithIvalidCountryLocationShouldReturnBadRequest() throws Exception {

        // Invalid ISO 3166 Country Code (com.liferay.restaurants.infraestructure.services.IsoCountryCodeValidatorImpl)
        this.mvc.perform(post("/api/restaurants")
                .contentType(APPLICATION_JSON)
                .content(Files.readAllBytes(Paths.get(getClass().getResource("/japanese-invalid-location-country.json").toURI()))))
                .andDo(print())
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.status", is(400)))
                .andExpect(jsonPath("$.message", is("validation error")));
    }

    @Test
    public void createRestaurantWithIvalidUSALocationShouldReturnBadRequest() throws Exception {

        // Invalid USA Postal Code ("^[0-9]{5}(?:-[0-9]{4})?$";)
        this.mvc.perform(post("/api/restaurants")
                .contentType(APPLICATION_JSON)
                .content(Files.readAllBytes(Paths.get(getClass().getResource("/japanese-invalid-location-USA-postalCode.json").toURI()))))
                .andDo(print())
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.status", is(400)))
                .andExpect(jsonPath("$.message", is("validation error")));

        // Invalid USA State (com.liferay.restaurants.application.UsaStates)
        this.mvc.perform(post("/api/restaurants")
                .contentType(APPLICATION_JSON)
                .content(Files.readAllBytes(Paths.get(getClass().getResource("/japanese-invalid-location-USA-state.json").toURI()))))
                .andDo(print())
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.status", is(400)))
                .andExpect(jsonPath("$.message", is("validation error")));
    }

    @Test
    public void createRestaurantWithEmptyPhoneShouldReturnBadRequest() throws Exception {

        // Phone mandatory
        this.mvc.perform(post("/api/restaurants")
                .contentType(APPLICATION_JSON)
                .content(Files.readAllBytes(Paths.get(getClass().getResource("/japanese-invalid-noPhone.json").toURI()))))
                .andDo(print())
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.status", is(400)))
                .andExpect(jsonPath("$.message", is("validation error")));
    }

    @Test
    public void createRestaurantWithInvalidFormatPhoneShouldReturnBadRequest() throws Exception {

        // Invalid phone format (com.liferay.restaurants.infraestructure.services.GooglePhoneNumberParser)
        this.mvc.perform(post("/api/restaurants")
                .contentType(APPLICATION_JSON)
                .content(Files.readAllBytes(Paths.get(getClass().getResource("/japanese-invalid-phoneFormatError.json").toURI()))))
                .andDo(print())
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.status", is(400)))
                .andExpect(jsonPath("$.message", is("validation error")));
    }

    @Test
    public void createRestaurantWithEmptyNameShouldReturnBadRequest() throws Exception {

        // Name mandatory
        this.mvc.perform(post("/api/restaurants")
                .contentType(APPLICATION_JSON)
                .content(Files.readAllBytes(Paths.get(getClass().getResource("/japanese-invalid-noName.json").toURI()))))
                .andDo(print())
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.status", is(400)))
                .andExpect(jsonPath("$.message", is("validation error")));
    }

    @Test
    public void createRestaurantWithPriceOutOfRangeShouldReturnBadRequest() throws Exception {

        //Price between 0 and 5
        this.mvc.perform(post("/api/restaurants")
                .contentType(APPLICATION_JSON)
                .content(Files.readAllBytes(Paths.get(getClass().getResource("/japanese-invalid-negativePrice.json").toURI()))))
                .andDo(print())
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.status", is(400)))
                .andExpect(jsonPath("$.message", is("validation error")));

        this.mvc.perform(post("/api/restaurants")
                .contentType(APPLICATION_JSON)
                .content(Files.readAllBytes(Paths.get(getClass().getResource("/japanese-invalid-priceGreaterThan5.json").toURI()))))
                .andDo(print())
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.status", is(400)))
                .andExpect(jsonPath("$.message", is("validation error")));
    }

    @Test
    public void createRestaurantWithRatingOutOfRangeShouldReturnBadRequest() throws Exception {

        // Rating between 0 and 5
        this.mvc.perform(post("/api/restaurants")
                .contentType(APPLICATION_JSON)
                .content(Files.readAllBytes(Paths.get(getClass().getResource("/japanese-invalid-negativeRating.json").toURI()))))
                .andDo(print())
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.status", is(400)))
                .andExpect(jsonPath("$.message", is("validation error")));

        this.mvc.perform(post("/api/restaurants")
                .contentType(APPLICATION_JSON)
                .content(Files.readAllBytes(Paths.get(getClass().getResource("/japanese-invalid-ratingGreaterThan5.json").toURI()))))
                .andDo(print())
                .andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.status", is(400)))
                .andExpect(jsonPath("$.message", is("validation error")));

    }

    @Test
    public void getExistingRestaurantShoukdReturnOk() throws Exception {

        given(applicationService.findRestaurant(VALID_RESTAURANT_ID))
                .willReturn(Optional.of(aValidRestaurant));

        mvc.perform(get("/api/restaurants/{id}", VALID_RESTAURANT_ID))
            .andExpect(status().isOk())
            .andExpect(content().contentType(MediaTypes.HAL_JSON_VALUE + ";charset=UTF-8"))
            .andDo(print())
            .andExpect(jsonPath("$.restaurantId", is(VALID_RESTAURANT_ID)))
            .andExpect(jsonPath("$.name", is(aValidRestaurant.getName())))
            .andExpect(jsonPath("$.description", is(aValidRestaurant.getDescription())))
            .andExpect(jsonPath("$.categories[0].name", is(aValidRestaurant.getCategories().get(0).getName())))
            .andExpect(jsonPath("$.categories[0].displayName", is(aValidRestaurant.getCategories().get(0).getDisplayName())))
            .andExpect(jsonPath("$.categories[1].name", is(aValidRestaurant.getCategories().get(1).getName())))
            .andExpect(jsonPath("$.categories[1].displayName", is(aValidRestaurant.getCategories().get(1).getDisplayName())))
            .andExpect(jsonPath("$.location.address1", is(aValidRestaurant.getLocation().getAddress1())))
            .andExpect(jsonPath("$.location.address2", is(aValidRestaurant.getLocation().getAddress2())))
            .andExpect(jsonPath("$.location.city", is(aValidRestaurant.getLocation().getCity())))
            .andExpect(jsonPath("$.location.postalCode", is(aValidRestaurant.getLocation().getPostalCode())))
            .andExpect(jsonPath("$.location.country", is(aValidRestaurant.getLocation().getCountry().getCode())))
            .andExpect(jsonPath("$.location.state", is(aValidRestaurant.getLocation().getState())))
            .andExpect(jsonPath("$.coordinates.latitude", is(aValidRestaurant.getCoordinates().getLat())))
            .andExpect(jsonPath("$.coordinates.longitude", is(aValidRestaurant.getCoordinates().getLon())))
            .andExpect(jsonPath("$.phone", is(aValidRestaurant.getPhone().getNumber())))
            .andExpect(jsonPath("$.internationalPhone", is(aValidRestaurant.getPhone().getInternationalNumber())))
            .andExpect(jsonPath("$.price", is(aValidRestaurant.getPrice().getFormattedValue())))
            .andExpect(jsonPath("$.reviewCount", is(aValidRestaurant.getRating().getReviewCount())))
            .andExpect(jsonPath("$.rating", is(aValidRestaurant.getRating().getValue())))
            .andExpect(jsonPath("$._links.self.href", is("http://localhost/api/restaurants/" + VALID_RESTAURANT_ID)));

        verify(applicationService, times(1)).findRestaurant(VALID_RESTAURANT_ID);
        verifyNoMoreInteractions(applicationService);
    }

    @Test
    public void getNotExistentRestaurantShoukdReturnNotFound() throws Exception {

        given(applicationService.findRestaurant(VALID_RESTAURANT_ID))
                .willReturn(Optional.empty());

        mvc.perform(get("/api/restaurants/{id}", VALID_RESTAURANT_ID))
                .andExpect(status().isNotFound());

        verify(applicationService, times(1)).findRestaurant(VALID_RESTAURANT_ID);
        verifyNoMoreInteractions(applicationService);
    }

    @Test
    public void getAllRestaurantsShouldReturnOk() throws Exception {

        when(applicationService.searchRestaurants(any(SearchRequest.class)))
                .thenReturn(new SearchResponse(new PageImpl<>(asList(aValidRestaurant, anotherValidRestaurant),
                        new PageRequest(0, 20, null), 2),
                        new SearchResponse.Aggregations()));

        when(applicationService.searchRestaurants(any(Pageable.class)))
                .thenReturn(new PageImpl<>(asList(aValidRestaurant, anotherValidRestaurant), new PageRequest(0, 20, null), 2));

        mvc.perform(get("/api/restaurants"))
                .andExpect(status().isOk())
                .andDo(print())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(jsonPath("$.data.page.totalElements", is(2)))
                .andExpect(jsonPath("$.data.content", hasSize(2)))
                .andExpect(jsonPath("$.data.content[0].restaurantId", is(aValidRestaurant.getId())))
                .andExpect(jsonPath("$.data.content[1].restaurantId", is(anotherValidRestaurant.getId())));

        verify(applicationService, times(1)).searchRestaurants(any(SearchRequest.class));
        verifyNoMoreInteractions(applicationService);
    }
}