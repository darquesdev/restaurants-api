package com.liferay.restaurants.infraestructure.services;

import com.liferay.restaurants.model.domain.restaurant.RestaurantIdGenerator;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.util.UUID;

/**
 * @author david.arques
 */
@Service("restaurantIdGenerator")
public class RestaurantIdGeneratorImpl implements RestaurantIdGenerator {

    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    public RestaurantIdGeneratorImpl() {
        logger.info("Creating logger RestaurantIdGeneratorImpl...");
    }

    @Override
    public String generateId() {
        logger.info("Generating restaurant id");
        String uniqueID = UUID.randomUUID().toString();
        logger.info("Generated restaurant id: {}", uniqueID);
        return uniqueID;
    }
}
