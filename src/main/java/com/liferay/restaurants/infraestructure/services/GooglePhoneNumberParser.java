package com.liferay.restaurants.infraestructure.services;

import com.google.i18n.phonenumbers.PhoneNumberUtil;
import com.google.i18n.phonenumbers.Phonenumber;
import com.liferay.restaurants.model.domain.restaurant.ParsedPhoneNumber;
import com.liferay.restaurants.model.domain.restaurant.PhoneNumberParser;
import com.liferay.restaurants.model.domain.restaurant.PhoneNumberParsingException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

/**
 * @author david.arques
 */
@Service("phoneNumberParser")
public class GooglePhoneNumberParser implements PhoneNumberParser {

    private static final String ERROR_MSG = "The phone number '%s' with country code '%s' is not valid";
    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    @Override
    public ParsedPhoneNumber parse(String aPhoneNumber, String aIsoCountryCode) {
        logger.debug("parsing a PhoneNumber {} with a IsoCountryCode {}", aPhoneNumber, aIsoCountryCode);
        try {
            PhoneNumberUtil phoneUtil = PhoneNumberUtil.getInstance();
            Phonenumber.PhoneNumber numberProto = phoneUtil.parse(aPhoneNumber, aIsoCountryCode.toUpperCase());
            if (!phoneUtil.isValidNumber(numberProto)) {
                throw new PhoneNumberParsingException(String.format(ERROR_MSG, aPhoneNumber, aIsoCountryCode));
            }

            return new ParsedPhoneNumber(
                    aIsoCountryCode,
                    numberProto.getCountryCode(),
                    numberProto.getNationalNumber(),
                    phoneUtil.format(numberProto, PhoneNumberUtil.PhoneNumberFormat.NATIONAL),
                    phoneUtil.format(numberProto, PhoneNumberUtil.PhoneNumberFormat.INTERNATIONAL));

        } catch (com.google.i18n.phonenumbers.NumberParseException e) {
            throw new PhoneNumberParsingException(String.format(ERROR_MSG, aPhoneNumber, aIsoCountryCode), e);
        }
    }

    @Override
    public boolean isValidPhoneNumber(String aPhoneNumber, String aIsoCountryCode) {
        try {
            this.parse(aPhoneNumber, aIsoCountryCode);
            return true;
        } catch (PhoneNumberParsingException e) {
            return false;
        }
    }
}
