package com.liferay.restaurants.infraestructure.services;

import com.liferay.restaurants.model.domain.restaurant.IsoCountryCodeValidator;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.HashSet;
import java.util.Locale;
import java.util.Set;

/**
 * @author david.arques
 */
@Service("isoCountryCodeValidator")
public class IsoCountryCodeValidatorImpl implements IsoCountryCodeValidator {

    private static final Set<String> ISO_COUNTRIES = new HashSet<>(Arrays.asList(Locale.getISOCountries()));
    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    @Override
    public boolean isValidIsoCountryCode(String anIsoCountryCode) {
        logger.debug("Validating IsoCountry {}", anIsoCountryCode);
        if (StringUtils.isEmpty(anIsoCountryCode)) {
            logger.debug("Iso Country Code empty");
            return false;
        }
        return ISO_COUNTRIES.contains(anIsoCountryCode.toUpperCase());
    }
}
