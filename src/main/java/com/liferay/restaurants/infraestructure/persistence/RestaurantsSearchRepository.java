package com.liferay.restaurants.infraestructure.persistence;

import com.liferay.restaurants.api.model.search.SearchRequest;
import com.liferay.restaurants.api.model.search.SearchResponse;

/**
 * @author david.arques
 */
public interface RestaurantsSearchRepository {

    SearchResponse search(SearchRequest request);
}
