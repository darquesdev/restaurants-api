package com.liferay.restaurants.infraestructure.persistence;

import com.liferay.restaurants.model.domain.restaurant.Restaurant;
import com.liferay.restaurants.model.domain.restaurant.RestaurantRepository;
import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

/**
 * @author david.arques
 */
public interface RestaurantElasticsearchRepository extends RestaurantRepository, ElasticsearchRepository<Restaurant, String> {
}
