package com.liferay.restaurants.infraestructure.persistence;

import com.liferay.restaurants.api.model.search.SearchRequest;
import com.liferay.restaurants.api.model.search.SearchResponse;
import org.elasticsearch.index.query.QueryBuilder;
import org.elasticsearch.search.aggregations.AbstractAggregationBuilder;
import org.elasticsearch.search.aggregations.Aggregations;
import org.elasticsearch.search.sort.SortBuilder;

/**
 * @author david.arques
 */
public interface ElasticsearchHelper {

    QueryBuilder createQueryBuilder(SearchRequest request);

    AbstractAggregationBuilder[] createAggregations();

    SearchResponse.Aggregations createAggregations(Aggregations value);

    SortBuilder[] createSorts(SearchRequest request);
}
