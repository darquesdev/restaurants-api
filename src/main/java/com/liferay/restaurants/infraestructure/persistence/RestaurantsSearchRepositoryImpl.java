package com.liferay.restaurants.infraestructure.persistence;

import com.liferay.restaurants.api.model.search.SearchRequest;
import com.liferay.restaurants.api.model.search.SearchResponse;
import com.liferay.restaurants.infraestructure.config.SearchProperties;
import com.liferay.restaurants.model.domain.restaurant.Restaurant;
import org.apache.commons.lang.ArrayUtils;
import org.elasticsearch.search.aggregations.AbstractAggregationBuilder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.elasticsearch.core.ElasticsearchTemplate;
import org.springframework.data.elasticsearch.core.aggregation.AggregatedPage;
import org.springframework.data.elasticsearch.core.query.NativeSearchQueryBuilder;
import org.springframework.data.elasticsearch.core.query.SearchQuery;
import org.springframework.stereotype.Repository;
import org.springframework.util.Assert;

/**
 * @author david.arques
 */
@Repository
public class RestaurantsSearchRepositoryImpl implements RestaurantsSearchRepository {

    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    private final ElasticsearchTemplate elasticsearchTemplate;
	private final ElasticsearchHelper helper;

    @Autowired
	public RestaurantsSearchRepositoryImpl(ElasticsearchTemplate elasticsearchTemplate,
                                           SearchProperties searchApiProperties) {
        this.elasticsearchTemplate = elasticsearchTemplate;
        this.helper = new ElasticsearchExtractorImpl(searchApiProperties);
    }

    @Override
    public SearchResponse search(SearchRequest request) {
		logger.info("Building Elasticsearch SearchQuery from Request: {}", request);
        Assert.notNull(request, "SearchRequest must not be null");
        SearchQuery searchQuery = prepareSearchQuery(request);
        AggregatedPage<Restaurant> aggregatedPage = elasticsearchTemplate.queryForPage(searchQuery, Restaurant.class);
		return new com.liferay.restaurants.api.model.search.SearchResponse(aggregatedPage, helper.createAggregations(aggregatedPage.getAggregations()));
	}

    private SearchQuery prepareSearchQuery(SearchRequest request){

        NativeSearchQueryBuilder searchQueryBuilder = new NativeSearchQueryBuilder()
                .withIndices(Restaurant.INDEX_NAME)
                .withTypes(Restaurant.TYPE)
                .withQuery(helper.createQueryBuilder(request))
                .withPageable(request.getPageAndSort());

        AbstractAggregationBuilder[] aggregations = helper.createAggregations();
        if (!ArrayUtils.isEmpty(aggregations)) {
            for (AbstractAggregationBuilder aggregation : aggregations) {
                searchQueryBuilder.addAggregation(aggregation);
            }
        }

        SearchQuery searchQuery = searchQueryBuilder.build();
        logger.info("SearchQuery ready {}", searchQuery);
        return searchQuery;
	}
}