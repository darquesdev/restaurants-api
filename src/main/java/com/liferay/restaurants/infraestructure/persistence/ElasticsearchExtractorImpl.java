package com.liferay.restaurants.infraestructure.persistence;

import com.liferay.restaurants.api.model.search.SearchRequest;
import com.liferay.restaurants.api.model.search.SearchResponse;
import com.liferay.restaurants.infraestructure.config.SearchProperties;
import org.apache.commons.lang.ArrayUtils;
import org.apache.commons.lang.StringUtils;
import org.elasticsearch.common.unit.DistanceUnit;
import org.elasticsearch.index.query.*;
import org.elasticsearch.search.aggregations.AbstractAggregationBuilder;
import org.elasticsearch.search.aggregations.Aggregations;
import org.elasticsearch.search.aggregations.bucket.terms.Terms;
import org.elasticsearch.search.sort.SortBuilder;
import org.elasticsearch.search.sort.SortBuilders;
import org.elasticsearch.search.sort.SortOrder;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.data.domain.Sort;
import org.springframework.util.Assert;
import org.springframework.util.CollectionUtils;

import java.util.ArrayList;
import java.util.List;

import static org.elasticsearch.index.query.QueryBuilders.geoDistanceQuery;
import static org.elasticsearch.index.query.QueryBuilders.matchAllQuery;
import static org.elasticsearch.index.query.QueryBuilders.multiMatchQuery;
import static org.elasticsearch.search.aggregations.AggregationBuilders.terms;

/**
 * @author david.arques
 */
public class ElasticsearchExtractorImpl implements ElasticsearchHelper {

    private static final int DEFAULT_RADIUS_DISTANCE = 50000;//meters
    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    private final SearchProperties searchProperties;

    public ElasticsearchExtractorImpl(SearchProperties searchProperties) {
        logger.info("Creating ElasticsearchExtractorImpl...");
        Assert.notNull(searchProperties, "Search Properties must not be null");
        this.searchProperties = searchProperties;
    }

    @Override
    public QueryBuilder createQueryBuilder(SearchRequest request) {

        logger.info("Creating QueryBuilder from request");

        Assert.notNull(request, "SearchRequest must not be null");

        BoolQueryBuilder boolQueryBuilder = new BoolQueryBuilder();
        boolQueryBuilder.must(createFulltextQueryBuilder(request));

        if (request.getFilter() == null) {
            logger.info("No filters found");
            return boolQueryBuilder;
        }

        SearchRequest.Filter filters = request.getFilter();

        if (!ArrayUtils.isEmpty(filters.getCategories())) {
            boolQueryBuilder.must(QueryBuilders.termsQuery("categories.name", filters.getCategories()));
        }

        if(filters.getLatitude().isPresent() && filters.getLongitude().isPresent()){
            boolQueryBuilder.must(geoDistanceQuery("coordinates")
                    .point(filters.getLatitude().get(), filters.getLongitude().get())
                    .distance(filters.getRadius().orElse(DEFAULT_RADIUS_DISTANCE), DistanceUnit.METERS));
        }

        if(!ArrayUtils.isEmpty(filters.getPrice())){
            boolQueryBuilder.must(QueryBuilders.termsQuery("price.value", filters.getPrice()));
        }

        if(!ArrayUtils.isEmpty(filters.getRating())){
            boolQueryBuilder.must(QueryBuilders.termsQuery("rating.range", filters.getRating()));
        }

        if(!ArrayUtils.isEmpty(filters.getCountries())){
            boolQueryBuilder.must(QueryBuilders.termsQuery("location.country.code", filters.getCountries()));
        }

        if(!ArrayUtils.isEmpty(filters.getCities())){
            boolQueryBuilder.must(QueryBuilders.termsQuery("location.city", filters.getCities()));
        }

        if(!ArrayUtils.isEmpty(filters.getStates())){
            boolQueryBuilder.must(QueryBuilders.termsQuery("location.state", filters.getStates()));
        }

        logger.info("QueryBuilder created {}", boolQueryBuilder);

        return boolQueryBuilder;
    }

    @Override
    public AbstractAggregationBuilder[] createAggregations() {
        logger.info("Extracting Aggregations from request properties");
        List<AbstractAggregationBuilder> result = new ArrayList<>();
        for (String aggregationField : searchProperties.getAggregationFields()) {
            result.add(terms(aggregationField).field(aggregationField));
        }
        logger.info("Aggregations extracted {}", result);
        return result.toArray(new AbstractAggregationBuilder[result.size()]);
    }

    @Override
    public SearchResponse.Aggregations createAggregations(Aggregations value) {

        SearchResponse.Aggregations aggregations = new SearchResponse.Aggregations();

        for (String termName : searchProperties.getAggregationFields()) {

            Terms tipos = value.get(termName);

            SearchResponse.Aggregation aggregation = new SearchResponse.Aggregation();

            // For each entry
            for (Terms.Bucket entry : tipos.getBuckets()) {
                entry.getKey();      // Term
                entry.getDocCount(); // Doc count
                aggregation.put(entry.getKeyAsString(), entry.getDocCount());
            }

            aggregations.put(getAggregationName(termName), aggregation);
        }
        return aggregations;
    }


    private String getAggregationName(String fieldName) {
        return fieldName.lastIndexOf(".") == -1 ? fieldName :
                fieldName.substring(0, fieldName.lastIndexOf("."));
    }

    @Override
    public SortBuilder[] createSorts(SearchRequest request) {

        List<SortBuilder> result = new ArrayList<>();

        boolean sortExists = (request.getPageAndSort() != null)
                && (request.getPageAndSort().getSort() != null);

        if (sortExists) {
            for (Sort.Order order : request.getPageAndSort().getSort()) {
                result.add(SortBuilders.fieldSort(order.getProperty()).order(order.getDirection() == Sort.Direction.DESC
                        ? SortOrder.DESC
                        : SortOrder.ASC));
            }
        }

        return result.toArray(new SortBuilder[result.size()]);
    }


    /**
     * <p>
     * Build a query builder depending on the input term. If it is null return {@link MatchAllQueryBuilder};
     * if not {@link MultiMatchQueryBuilder}
     * </p>
     *
     * @param request search query
     * @return Elasticsearch Query Builder
     */
    private QueryBuilder createFulltextQueryBuilder(SearchRequest request) {

        logger.info("Creating Fulltext QueryBuilder from request");

        if (StringUtils.isEmpty(request.getTerm())) {
            logger.info("Query with match-all");
            return matchAllQuery();
        }

        if(logger.isInfoEnabled()){
            logger.info("Fulltext with term {} and fields {}", request.getTerm(), ArrayUtils.toString(searchProperties.getFulltextFields()));
        }

        return multiMatchQuery(request.getTerm(), searchProperties.getFulltextFields());
    }

}
