package com.liferay.restaurants.infraestructure.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import springfox.documentation.service.ApiInfo;
import springfox.documentation.service.Contact;
import springfox.documentation.spi.DocumentationType;
import springfox.documentation.spring.web.plugins.Docket;
import springfox.documentation.swagger2.annotations.EnableSwagger2;

import java.util.Collections;

import static springfox.documentation.builders.PathSelectors.regex;

/**
 * @author david.arques
 */
@Profile("default")
@Configuration
@EnableSwagger2
public class SwaggerConfig {

    private ApiInfo apiInfo() {
        return new ApiInfo(
                "The Restaurants API",
                "Liferay Backend Engineer Exercise",
                "API 1.0",
                null,
                new Contact("David Arques", null, "davidarchena@gmail.com"),
                null,
                null,
                Collections.emptyList());
    }

    @Bean
    public Docket listingApi() {
        return new Docket(DocumentationType.SWAGGER_2)
                .groupName("restaurant")
                .apiInfo(apiInfo())
                .select()
                .paths(regex("/api/restaurants.*"))
                .build();
    }
}

