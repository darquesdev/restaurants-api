package com.liferay.restaurants.infraestructure.config;

import com.liferay.restaurants.application.AsyncEventListener;
import com.liferay.restaurants.application.SyncEventListener;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Profile;
import org.springframework.scheduling.annotation.EnableAsync;

/**
 * @author david.arques
 */
@EnableAsync
@Configuration
public class EventListenerConfig {

    @Profile("test")
    @Bean
    @Autowired
    public SyncEventListener shippingSyncEventListener(){
        return new SyncEventListener();
    }

    @Profile("default")
    @Bean
    @Autowired
    public AsyncEventListener shippingAsyncEventListener(){
        return new AsyncEventListener();
    }

}
