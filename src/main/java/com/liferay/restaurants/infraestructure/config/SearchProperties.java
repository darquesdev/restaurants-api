package com.liferay.restaurants.infraestructure.config;

import org.springframework.boot.context.properties.ConfigurationProperties;
import org.springframework.context.annotation.Configuration;

/**
 * @author david.arques
 */
@ConfigurationProperties(prefix = "restaurants.search")
@Configuration
public class SearchProperties {

    private String[] fulltextFields;
    private String[] aggregationFields;

    public String[] getFulltextFields() {
        return fulltextFields;
    }

    public void setFulltextFields(String[] fulltextFields) {
        this.fulltextFields = fulltextFields;
    }

    public String[] getAggregationFields() {
        return aggregationFields;
    }

    public void setAggregationFields(String[] aggregationFields) {
        this.aggregationFields = aggregationFields;
    }
}
