package com.liferay.restaurants.application;

import com.liferay.restaurants.api.model.search.SearchRequest;
import com.liferay.restaurants.api.model.search.SearchResponse;
import com.liferay.restaurants.infraestructure.persistence.RestaurantsSearchRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import javax.annotation.PostConstruct;

/**
 * @author david.arques
 */
@Service
public class SearchApplicationServiceImpl implements SearchApplicationService {

    private final Logger logger = LoggerFactory.getLogger(this.getClass());
    private final RestaurantsSearchRepository searchRepository;

    @Autowired
    public SearchApplicationServiceImpl(RestaurantsSearchRepository searchRepository) {
        logger.info("Creating SearchApplicationServiceImpl...");
        Assert.notNull(searchRepository, "RestaurantsSearchRepository is mandatory");
        this.searchRepository = searchRepository;
    }

    @PostConstruct
    private void init(){
        logger.info("SearchApplicationServiceImpl created");
    }

    @Override
    public SearchResponse searchRestaurants(SearchRequest request) {
        return searchRepository.search(request);
    }
}
