package com.liferay.restaurants.application;

import com.liferay.restaurants.api.model.RestaurantEditableDto;
import com.liferay.restaurants.api.model.search.SearchRequest;
import com.liferay.restaurants.api.model.search.SearchResponse;
import com.liferay.restaurants.model.domain.restaurant.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.util.Assert;

import javax.annotation.PostConstruct;
import java.util.Date;
import java.util.Optional;

import static com.liferay.restaurants.application.ModelMapper.dtoToModel;

/**
 * @author david.arques
 */
@Service
public class RestaurantsApplicationServiceImpl implements RestaurantsApplicationService {

    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    private final RestaurantRepository repository;
    private final EventPublisher publisher;
    private final RestaurantIdGenerator restaurantIdGenerator;
    private final SearchApplicationService searchApplicationService;

    @Autowired
    public RestaurantsApplicationServiceImpl(RestaurantRepository repository,
                                             EventPublisher publisher,
                                             RestaurantIdGenerator restaurantIdGenerator,
                                             SearchApplicationService searchApplicationService) {
        logger.info("Creating RestaurantsApplicationServiceImpl...");
        Assert.notNull(repository, " RestaurantRepository is mandatory");
        Assert.notNull(publisher, " EventPublisher is mandatory");
        Assert.notNull(restaurantIdGenerator, " RestaurantIdGenerator is mandatory");
        Assert.notNull(searchApplicationService, " RestaurantsApplicationServiceImpl is mandatory");
        this.searchApplicationService = searchApplicationService;
        this.repository = repository;
        this.publisher = publisher;
        this.restaurantIdGenerator = restaurantIdGenerator;
    }

    @PostConstruct
    public void init() {
        logger.info("RestaurantsApplicationServiceImpl created");
    }


    @Override
    public Optional<Restaurant> createRestaurant(RestaurantEditableDto aRestaurant) {

        logger.info("creating restaurant {}", aRestaurant);

        Restaurant newRestaurant = dtoToModel(restaurantIdGenerator.generateId(), aRestaurant)
                .modifiedAt(new Date())
                .createdAt(new Date())
                .build();

        Optional<Restaurant> createdRestaurant = Optional.ofNullable(repository.save(newRestaurant));
        createdRestaurant.ifPresent(restaurant -> {
            publisher.publish(new RestaurantCreated(restaurant.getId(), restaurant.getName()));
            logger.info("restaurant {} created", restaurant);
        });

        return createdRestaurant;
    }

    @Override
    public void deleteRestaurant(String aRestaurantId) {
        logger.info("deleting restaurant with id {}", aRestaurantId);
        if (!repository.exists(aRestaurantId)) {
            throw new RestaurantNotFoundException(aRestaurantId);
        }
        repository.delete(aRestaurantId);
        publisher.publish(new RestaurantDeleted(aRestaurantId));
        logger.info("restaurant with id {} deleted", aRestaurantId);
    }

    @Override
    public Optional<Restaurant> updateRestaurant(String restaurantId, RestaurantEditableDto aRestaurant) {
        logger.info("updating restaurant {}", aRestaurant);

        if (!repository.exists(restaurantId)) {
            throw new RestaurantNotFoundException(restaurantId);
        }

        Restaurant oldrestaurant = repository.findOne(restaurantId);
        Restaurant newRestaurant = dtoToModel(restaurantId, aRestaurant)
                .modifiedAt(new Date())
                .createdAt(oldrestaurant.getCreatedAt())
                .build();
        Restaurant updatedRestaurant = repository.save(newRestaurant);
        publisher.publish(new RestaurantUpdated(restaurantId));
        logger.info("restaurant with id {} updated", restaurantId);
        return Optional.ofNullable(updatedRestaurant);
    }

    @Override
    public SearchResponse searchRestaurants(SearchRequest request) {
        logger.info("searching restaurants");
        Assert.notNull(request, "SearchRequest is mandatory");
        SearchResponse response = searchApplicationService.searchRestaurants(request);
        logger.info("total {}, returning {}", response.getData().getTotalElements(), response.getData().getNumberOfElements());
        return response;
    }

    @Override
    public Page<Restaurant> searchRestaurants(Pageable page) {
        logger.info("getting restaurants");
        Page<Restaurant> results = repository.findAll(page);
        logger.info("total {}, returning {}", results.getTotalElements(), results.getNumberOfElements());
        return results;
    }

    @Override
    public Optional<Restaurant> findRestaurant(String aRestaurantId) {
        logger.info("getting restaurant {}", aRestaurantId);
        Restaurant restaurant = repository.findOne(aRestaurantId);
        logger.info("returning restaurant {}", restaurant);
        return Optional.ofNullable(restaurant);
    }

    @Override
    public boolean restaurantExists(String aRestaurantId) {
        return repository.exists(aRestaurantId);
    }
}
