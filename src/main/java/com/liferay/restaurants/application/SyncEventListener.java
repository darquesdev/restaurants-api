package com.liferay.restaurants.application;

import com.liferay.restaurants.model.domain.restaurant.RestaurantEvent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.event.EventListener;

import javax.annotation.PostConstruct;

/**
 * @author david.arques
 */
public class SyncEventListener {

    private static final Logger logger = LoggerFactory.getLogger(SyncEventListener.class);

    public SyncEventListener() {
        logger.info("Creating SyncEventListener...");
    }

    @PostConstruct
    private void init(){
        logger.info("SyncEventListener created");
    }

    @EventListener
    public void handleEvent(RestaurantEvent event) {
        logger.info("Handling event {}", event);
    }
}
