package com.liferay.restaurants.application;

import com.liferay.restaurants.api.model.RestaurantEditableDto;
import com.liferay.restaurants.api.model.search.SearchRequest;
import com.liferay.restaurants.api.model.search.SearchResponse;
import com.liferay.restaurants.model.domain.restaurant.Restaurant;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

import java.util.Optional;

/**
 * @author david.arques
 */
public interface RestaurantsApplicationService {

    Optional<Restaurant> createRestaurant(RestaurantEditableDto aRestaurant);

    void deleteRestaurant(String aRestaurantId);

    Optional<Restaurant> updateRestaurant(String restaurantId, RestaurantEditableDto aRestaurant);

    SearchResponse searchRestaurants(SearchRequest request);

    Page<Restaurant> searchRestaurants(Pageable page);

    Optional<Restaurant> findRestaurant(String aRestaurantId);

    boolean restaurantExists(String aRestaurantId);
}
