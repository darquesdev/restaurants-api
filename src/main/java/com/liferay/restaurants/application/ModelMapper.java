package com.liferay.restaurants.application;

import com.liferay.restaurants.api.model.CategoryDto;
import com.liferay.restaurants.api.model.LocationDto;
import com.liferay.restaurants.api.model.RestaurantEditableDto;
import com.liferay.restaurants.model.domain.restaurant.*;
import org.springframework.util.Assert;

import java.util.Collection;
import java.util.Collections;
import java.util.Optional;

/**
 * @author david.arques
 */
public class ModelMapper {

    private ModelMapper() {
    }

    static Category[] dtoToModel(Collection<CategoryDto> categories) {
        return Optional.ofNullable(categories)
                .orElse(Collections.emptySet())
                .stream()
                .map(dto -> new Category(dto.getName(), dto.getDisplayName()))
                .toArray(Category[]::new);
    }

    static Location dtoToModel(LocationDto dto) {
        Assert.notNull(dto, "LocationDto is mandatory");
        return new Location(dto.getAddress1(), dto.getAddress2(), dto.getPostalCode(),
                new IsoCountry(dto.getCountry()), dto.getCity(), dto.getState());
    }

    static RestaurantBuilder dtoToModel(String newRestaurantId, RestaurantEditableDto aRestaurant) {
        return Restaurant.builder(newRestaurantId)
                .withName(aRestaurant.getName())
                .withLocation(dtoToModel(aRestaurant.getLocation()))
                .withCategories(dtoToModel(aRestaurant.getCategories()))
                .withDescription(aRestaurant.getDescription())
                .withCoordinates(aRestaurant.getCoordinates().getLatitude(), aRestaurant.getCoordinates().getLongitude())
                .withPhone(aRestaurant.getPhone().getNumber(), aRestaurant.getPhone().getCountryCode())
                .withPrice(aRestaurant.getPrice())
                .withRating(aRestaurant.getRating(), aRestaurant.getReviewCount());
    }

}
