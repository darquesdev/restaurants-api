package com.liferay.restaurants.application;

import com.liferay.restaurants.model.domain.common.DomainEvent;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.stereotype.Component;

/**
 * @author david.arques
 */
@Component
public class EventPublisher {

    private final Logger logger = LoggerFactory.getLogger(this.getClass());
    private final ApplicationEventPublisher publisher;

    @Autowired
    public EventPublisher(ApplicationEventPublisher publisher) {
        this.publisher = publisher;
    }

    public void publish(DomainEvent anAppEvent) {
        logger.info("publishing event {}", anAppEvent);
        publisher.publishEvent(anAppEvent);
    }
}
