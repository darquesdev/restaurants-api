package com.liferay.restaurants.application;

import com.liferay.restaurants.model.domain.restaurant.IsoCountry;
import org.apache.commons.lang.StringUtils;

import java.util.Locale;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * TODO: Convert into @Service and include it into DomainRegistry
 */
public class UsaAddressValidator {

    private static final String USA_POSTAL_CODE_REGEX = "^[0-9]{5}(?:-[0-9]{4})?$";

    private UsaAddressValidator() {
    }

    public static boolean isValidUsaState(String state) {
        if (StringUtils.isEmpty(state)) {
            return false;
        }
        UsaStates usaStates = UsaStates.valueOfAbbreviation(state);
        return usaStates.equals(UsaStates.UNKNOWN);
    }

    public static boolean isValidUsaPostalCode(String postalCode) {
        if (StringUtils.isEmpty(postalCode)) {
            return false;
        }
        Pattern pattern = Pattern.compile(USA_POSTAL_CODE_REGEX);
        Matcher matcher = pattern.matcher(postalCode);
        return matcher.matches();
    }

    public static boolean isUsa(IsoCountry country) {
        return Locale.US.getCountry().equals(country.getCode());
    }

    public static boolean isUsa(String countryCode) {
        return Locale.US.getCountry().equals(countryCode);
    }
}
