package com.liferay.restaurants.application;

import com.liferay.restaurants.api.model.search.SearchRequest;
import com.liferay.restaurants.api.model.search.SearchResponse;

/**
 * @author david.arques
 */
public interface SearchApplicationService {

    SearchResponse searchRestaurants(SearchRequest request);
}
