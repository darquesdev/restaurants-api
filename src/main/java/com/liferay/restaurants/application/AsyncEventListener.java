package com.liferay.restaurants.application;

import com.liferay.restaurants.model.domain.restaurant.RestaurantCreated;
import com.liferay.restaurants.model.domain.restaurant.RestaurantDeleted;
import com.liferay.restaurants.model.domain.restaurant.RestaurantEvent;
import com.liferay.restaurants.model.domain.restaurant.RestaurantUpdated;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.context.event.EventListener;
import org.springframework.scheduling.annotation.Async;

import javax.annotation.PostConstruct;
import java.util.concurrent.TimeUnit;

/**
 * @author david.arques
 */
public class AsyncEventListener {

    private static final Logger logger = LoggerFactory.getLogger(AsyncEventListener.class);

    public AsyncEventListener() {
        logger.info("Creating AsyncEventListener...");
    }

    @PostConstruct
    private void init(){
        logger.info("AsyncEventListener created");
    }

    @Async
    @EventListener
    public void handleEvent(RestaurantCreated event) throws InterruptedException {
        logger.info("Handling event {}", event);

        //TODO Here we can call an external service in order to provide extra info (coodinates, rating, etc))
        logger.info("************ CALLING EXTERNAL SERVICE (restaurant={}) **", event.getRestaurantName());
        TimeUnit.SECONDS.sleep(1);
        logger.info("************ RETRIEVING DATA *****************************************");
        logger.info("************ UPDATING REPOSITORY *************************************");

    }

    @Async
    @EventListener
    public void handleEvent(RestaurantUpdated event) {
        logger.info("Restaurant updated {}", event);
    }

    @Async
    @EventListener
    public void handleEvent(RestaurantDeleted event) {
        logger.info("Restaurant deleted {}", event);
    }
}
