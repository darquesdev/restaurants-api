package com.liferay.restaurants;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

/**
 * @author david.arques
 */
@SpringBootApplication
public class RestaurantsApp {

    public static void main(String[] args) {
        SpringApplication.run(RestaurantsApp.class, args);
    }
}
