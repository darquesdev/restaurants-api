package com.liferay.restaurants.api.model;

import java.util.ArrayList;
import java.util.List;

/**
 * @author david.arques
 */
public final class RestaurantEditableDtoBuilder {

    private String name;
    private String description;
    private LocationDto location;
    private CoordinatesDto coordinates;
    private PhoneEditableDto phone;
    private List<CategoryDto> categories = new ArrayList<>();
    private int price;
    private int reviewCount;
    private double rating;

    protected RestaurantEditableDtoBuilder() {
    }

    public RestaurantEditableDtoBuilder withName(String name) {
        this.name = name;
        return this;
    }

    public RestaurantEditableDtoBuilder withDescription(String decription) {
        this.description = decription;
        return this;
    }

    public RestaurantEditableDtoBuilder withLocation(LocationDto location) {
        this.location = location;
        return this;
    }

    public RestaurantEditableDtoBuilder withCoordinates(double latitude, double longitude) {
        this.coordinates = new CoordinatesDto(latitude, longitude);
        return this;
    }

    public RestaurantEditableDtoBuilder withPhone(String number, String countryCode) {
        this.phone = new PhoneEditableDto(number, countryCode);
        return this;
    }

    public RestaurantEditableDtoBuilder withCategories(List<CategoryDto> categories) {
        this.categories = categories;
        return this;
    }

    public RestaurantEditableDtoBuilder withPrice(int price) {
        this.price = price;
        return this;
    }

    public RestaurantEditableDtoBuilder withReviewCount(int reviewCount) {
        this.reviewCount = reviewCount;
        return this;
    }

    public RestaurantEditableDtoBuilder withRating(double rating) {
        this.rating = rating;
        return this;
    }

    public RestaurantEditableDto build() {
        RestaurantEditableDto restaurantEditableDto = new RestaurantEditableDto();
        restaurantEditableDto.setName(name);
        restaurantEditableDto.setDescription(description);
        restaurantEditableDto.setLocation(location);
        restaurantEditableDto.setCoordinates(coordinates);
        restaurantEditableDto.setPhone(phone);
        restaurantEditableDto.setCategories(categories);
        restaurantEditableDto.setPrice(price);
        restaurantEditableDto.setReviewCount(reviewCount);
        restaurantEditableDto.setRating(rating);
        return restaurantEditableDto;
    }
}
