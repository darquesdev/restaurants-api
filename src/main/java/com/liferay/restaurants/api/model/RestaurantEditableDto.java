package com.liferay.restaurants.api.model;

import com.liferay.restaurants.api.model.validation.Location;
import com.liferay.restaurants.api.model.validation.Phone;
import com.liferay.restaurants.model.domain.restaurant.Restaurant;
import org.hibernate.validator.constraints.NotEmpty;

import javax.validation.Valid;
import javax.validation.constraints.Max;
import javax.validation.constraints.Min;
import javax.validation.constraints.NotNull;
import java.io.Serializable;
import java.util.*;
import java.util.stream.Collectors;

/**
 * @author david.arques
 */
public class RestaurantEditableDto implements Serializable {

    @NotEmpty
    private String name;

    private String description;

    @NotNull
    @Valid
    @Location
    private LocationDto location;

    private CoordinatesDto coordinates;

    @Valid
    @NotNull
    @Phone
    private PhoneEditableDto phone;

    @Valid
    private List<CategoryDto> categories = new ArrayList<>();

    @Min(0) @Max(5)
    private int price;

    private int reviewCount;

    @Min(0) @Max(5)
    private double rating;

    public static RestaurantEditableDto fromModel(Restaurant aRestaurant) {

        RestaurantEditableDto dto = new RestaurantEditableDto();

        dto.name = aRestaurant.getName();
        dto.description = aRestaurant.getDescription();
        dto.categories = Optional.ofNullable(aRestaurant.getCategories())
                .orElse(Collections.emptyList())
                .stream()
                .map(CategoryDto::fromModel).collect(Collectors.toList());

        dto.location = LocationDto.fromModel(aRestaurant.getLocation());

        dto.coordinates = Optional.ofNullable(aRestaurant.getCoordinates())
                .map(CoordinatesDto::fromModel).orElse(null);

        if(aRestaurant.getPhone() != null){
            dto.phone = new PhoneEditableDto(aRestaurant.getPhone().getNumber(), aRestaurant.getPhone().getIsoCountryCode());
        }

        dto.price = aRestaurant.getPrice() == null ? 0 : aRestaurant.getPrice().getValue();

        if(aRestaurant.getRating() != null){
            dto.reviewCount = aRestaurant.getRating().getReviewCount();
            dto.rating = aRestaurant.getRating().getValue();
        }

        return dto;
    }

    public static RestaurantEditableDtoBuilder builder() {
        return new RestaurantEditableDtoBuilder();
    }

    public LocationDto getLocation() {
        return this.location;
    }

    public void setLocation(LocationDto location) {
        this.location = location;
    }

    public List<CategoryDto> getCategories() {
        return this.categories;
    }

    public void setCategories(List<CategoryDto> categories) {
        this.categories = categories;
    }

    public String getName() {
        return this.name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getPrice() {
        return this.price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public int getReviewCount() {
        return this.reviewCount;
    }

    public void setReviewCount(int reviewCount) {
        this.reviewCount = reviewCount;
    }

    public double getRating() {
        return this.rating;
    }

    public void setRating(double rating) {
        this.rating = rating;
    }

    public CoordinatesDto getCoordinates() {
        return this.coordinates;
    }

    public void setCoordinates(CoordinatesDto coordinates) {
        this.coordinates = coordinates;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public PhoneEditableDto getPhone() {
        return phone;
    }

    public void setPhone(PhoneEditableDto phone) {
        this.phone = phone;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        RestaurantEditableDto that = (RestaurantEditableDto) o;
        return price == that.price &&
                reviewCount == that.reviewCount &&
                Double.compare(that.rating, rating) == 0 &&
                Objects.equals(name, that.name) &&
                Objects.equals(description, that.description) &&
                Objects.equals(location, that.location) &&
                Objects.equals(coordinates, that.coordinates) &&
                Objects.equals(phone, that.phone) &&
                Objects.equals(categories, that.categories);
    }

    @Override
    public int hashCode() {

        return Objects.hash(name, description, location, coordinates, phone, categories, price, reviewCount, rating);
    }

    @Override
    public String toString() {
        return "RestaurantEditable{" +
                "name='" + name + '\'' +
                ", description='" + description + '\'' +
                ", location=" + location +
                ", coordinates=" + coordinates +
                ", phone=" + phone +
                ", categories=" + categories +
                ", price=" + price +
                ", reviewCount=" + reviewCount +
                ", rating=" + rating +
                '}';
    }
}
