package com.liferay.restaurants.api.model.validation;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.*;

@Documented
@Constraint(validatedBy = LocationValidator.class)
@Target({ElementType.METHOD, ElementType.FIELD})
@Retention(RetentionPolicy.RUNTIME)
public @interface Location {

    String message() default "{location.error}";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}