package com.liferay.restaurants.api.model.validation;

import com.liferay.restaurants.api.model.PhoneEditableDto;
import com.liferay.restaurants.model.domain.restaurant.DomainRegistry;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class PhoneValidator implements ConstraintValidator<Phone, PhoneEditableDto> {

    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    @Override
    public void initialize(Phone constraintAnnotation) {
        logger.info("Initializing PhoneContactValidator...");
    }

    @Override
    public boolean isValid(PhoneEditableDto aPhoneNumber, ConstraintValidatorContext cxt) {
        logger.info("Validating phone number {}", aPhoneNumber);

        return aPhoneNumber != null
                && DomainRegistry.phoneNumberParser().isValidPhoneNumber(aPhoneNumber.getNumber(), aPhoneNumber.getCountryCode());
    }

}