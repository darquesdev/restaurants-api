package com.liferay.restaurants.api.model;

/**
 * @author david.arques
 */
public final class LocationDtoBuilder {

    private String address1;
    private String address2;
    private String city;
    private String postalCode;
    private String country;
    private String state;

    protected LocationDtoBuilder() {
    }

    public LocationDtoBuilder withAddress1(String address1) {
        this.address1 = address1;
        return this;
    }

    public LocationDtoBuilder withAddress2(String address2) {
        this.address2 = address2;
        return this;
    }

    public LocationDtoBuilder withCity(String city) {
        this.city = city;
        return this;
    }

    public LocationDtoBuilder withPostalCode(String postalCode) {
        this.postalCode = postalCode;
        return this;
    }

    public LocationDtoBuilder withCountry(String country) {
        this.country = country;
        return this;
    }

    public LocationDtoBuilder withState(String state) {
        this.state = state;
        return this;
    }

    public LocationDto build() {
        return new LocationDto(address1, address2, city, postalCode, country, state);
    }
}
