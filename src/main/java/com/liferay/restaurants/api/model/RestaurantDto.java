package com.liferay.restaurants.api.model;

import com.liferay.restaurants.model.domain.restaurant.Restaurant;
import org.springframework.hateoas.ResourceSupport;

import java.io.Serializable;
import java.util.Collections;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.stream.Collectors;

/**
 * @author david.arques
 */
public class RestaurantDto extends ResourceSupport implements Serializable{

    private String restaurantId;
    private String name;
    private String description;
    private List<CategoryDto> categories;
    private LocationDto location;
    private CoordinatesDto coordinates;
    private String phone;
    private String internationalPhone;
    private String price;
    private int reviewCount;
    private double rating;

    public static RestaurantDto fromModel(Restaurant aRestaurant) {

        RestaurantDto dto = new RestaurantDto();

        dto.restaurantId = aRestaurant.getId();
        dto.name = aRestaurant.getName();
        dto.description = aRestaurant.getDescription();
        dto.categories = Optional.ofNullable(aRestaurant.getCategories())
            .orElse(Collections.emptyList())
            .stream()
            .map(CategoryDto::fromModel).collect(Collectors.toList());

        dto.location = Optional.ofNullable(aRestaurant.getLocation())
                .map(LocationDto::fromModel).orElse(null);

        dto.coordinates = Optional.ofNullable(aRestaurant.getCoordinates())
            .map(CoordinatesDto::fromModel).orElse(null);

        if(aRestaurant.getPhone() != null){
            dto.phone = aRestaurant.getPhone().getNumber();
            dto.internationalPhone = aRestaurant.getPhone().getInternationalNumber();
        }

        dto.price = aRestaurant.getPrice() == null ? null : aRestaurant.getPrice().getFormattedValue();

        if(aRestaurant.getRating() != null){
            dto.reviewCount = aRestaurant.getRating().getReviewCount();
            dto.rating = aRestaurant.getRating().getValue();
        }

        return dto;
    }

    public String getRestaurantId() {
        return restaurantId;
    }

    public void setRestaurantId(String restaurantId) {
        this.restaurantId = restaurantId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public List<CategoryDto> getCategories() {
        return categories;
    }

    public void setCategories(List<CategoryDto> categories) {
        this.categories = categories;
    }

    public LocationDto getLocation() {
        return location;
    }

    public void setLocation(LocationDto location) {
        this.location = location;
    }

    public CoordinatesDto getCoordinates() {
        return coordinates;
    }

    public void setCoordinates(CoordinatesDto coordinates) {
        this.coordinates = coordinates;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public String getInternationalPhone() {
        return internationalPhone;
    }

    public void setInternationalPhone(String internationalPhone) {
        this.internationalPhone = internationalPhone;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public int getReviewCount() {
        return reviewCount;
    }

    public void setReviewCount(int reviewCount) {
        this.reviewCount = reviewCount;
    }

    public double getRating() {
        return rating;
    }

    public void setRating(double rating) {
        this.rating = rating;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        if (!super.equals(o)) return false;
        RestaurantDto that = (RestaurantDto) o;
        return reviewCount == that.reviewCount &&
                Double.compare(that.rating, rating) == 0 &&
                Objects.equals(restaurantId, that.restaurantId) &&
                Objects.equals(name, that.name) &&
                Objects.equals(description, that.description) &&
                Objects.equals(categories, that.categories) &&
                Objects.equals(location, that.location) &&
                Objects.equals(coordinates, that.coordinates) &&
                Objects.equals(phone, that.phone) &&
                Objects.equals(internationalPhone, that.internationalPhone) &&
                Objects.equals(price, that.price);
    }

    @Override
    public int hashCode() {

        return Objects.hash(super.hashCode(), restaurantId, name, description, categories, location, coordinates, phone, internationalPhone, price, reviewCount, rating);
    }

    @Override
    public String toString() {
        return "RestaurantDto{" +
                "restaurantId='" + restaurantId + '\'' +
                ", name='" + name + '\'' +
                ", description='" + description + '\'' +
                ", categories=" + categories +
                ", location=" + location +
                ", coordinates=" + coordinates +
                ", phone='" + phone + '\'' +
                ", internationalPhone='" + internationalPhone + '\'' +
                ", price='" + price + '\'' +
                ", reviewCount=" + reviewCount +
                ", rating=" + rating +
                '}';
    }
}
