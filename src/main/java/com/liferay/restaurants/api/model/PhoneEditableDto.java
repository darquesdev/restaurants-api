package com.liferay.restaurants.api.model;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.hibernate.validator.constraints.NotEmpty;

import java.io.Serializable;
import java.util.Objects;

/**
 * @author david.arques
 */
public class PhoneEditableDto implements Serializable {

    @NotEmpty
    private String number;
    private String countryCode;

    @JsonCreator
    public PhoneEditableDto(@JsonProperty("number") String number,
                            @JsonProperty("countryCode") String countryCode) {
        this.number = number;
        this.countryCode = countryCode;
    }

    public String getNumber() {
        return number;
    }
    public void setNumber(String number) {
        this.number = number;
    }

    public String getCountryCode() {
        return countryCode;
    }
    public void setCountryCode(String countryCode) {
        this.countryCode = countryCode;
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        PhoneEditableDto that = (PhoneEditableDto) o;
        return Objects.equals(number, that.number) &&
                Objects.equals(countryCode, that.countryCode);
    }

    @Override
    public int hashCode() {

        return Objects.hash(number, countryCode);
    }

    @Override
    public String toString() {
        return "PhoneEditableDto{" +
                "number='" + number + '\'' +
                ", countryCode='" + countryCode + '\'' +
                '}';
    }
}