package com.liferay.restaurants.api.model;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.liferay.restaurants.model.domain.restaurant.Location;
import org.hibernate.validator.constraints.NotEmpty;

import java.io.Serializable;
import java.util.Objects;

/**
 * @author david.arques
 */
public class LocationDto implements Serializable {

    @NotEmpty
    private String address1;
    private String address2;

    @NotEmpty
    private String city;

    @NotEmpty
    private String postalCode;

    @NotEmpty
    private String country;

    @NotEmpty
    private String state;

    @JsonCreator
    public LocationDto(@JsonProperty("address1") String address1,
                       @JsonProperty("address2") String address2,
                       @JsonProperty("city") String city,
                       @JsonProperty("postalCode") String postalCode,
                       @JsonProperty("country") String country,
                       @JsonProperty("state") String state) {
        this.address1 = address1;
        this.address2 = address2;
        this.city = city;
        this.postalCode = postalCode;
        this.country = country;
        this.state = state;
    }

    public static LocationDto fromModel(Location l){
        return new LocationDto(l.getAddress1(), l.getAddress2(), l.getCity(), l.getPostalCode(), l.getCountry().getCode(), l.getState());
    }

    public static LocationDtoBuilder builder() {
        return new LocationDtoBuilder();
    }

    public String getState() {
        return this.state;
    }
    public void setState(String state) {
        this.state = state;
    }

    public String getAddress2() {
        return this.address2;
    }
    public void setAddress2(String address2) {
        this.address2 = address2;
    }

    public String getPostalCode() {
        return this.postalCode;
    }
    public void setPostalCode(String postalCode) {
        this.postalCode = postalCode;
    }

    public String getCity() {
        return this.city;
    }
    public void setCity(String city) {
        this.city = city;
    }

    public String getCountry() {
        return this.country;
    }
    public void setCountry(String country) {
        this.country = country;
    }

    public String getAddress1() {
        return this.address1;
    }
    public void setAddress1(String address1) {
        this.address1 = address1;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        LocationDto that = (LocationDto) o;
        return Objects.equals(address1, that.address1) &&
                Objects.equals(address2, that.address2) &&
                Objects.equals(city, that.city) &&
                Objects.equals(postalCode, that.postalCode) &&
                Objects.equals(country, that.country) &&
                Objects.equals(state, that.state);
    }

    @Override
    public int hashCode() {

        return Objects.hash(address1, address2, city, postalCode, country, state);
    }

    @Override
    public String toString() {
        return "LocationDto{" +
                "address1='" + address1 + '\'' +
                ", address2='" + address2 + '\'' +
                ", city='" + city + '\'' +
                ", postalCode='" + postalCode + '\'' +
                ", country='" + country + '\'' +
                ", state='" + state + '\'' +
                '}';
    }


}
