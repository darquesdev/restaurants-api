package com.liferay.restaurants.api.model.search;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.liferay.restaurants.api.model.RestaurantDto;
import com.liferay.restaurants.model.domain.restaurant.Restaurant;
import org.springframework.hateoas.PagedResources;
import org.springframework.hateoas.Resource;

/**
 * @author david.arques
 */
public class SearchResponseResource {

    private final PagedResources<Resource<RestaurantDto>> data;
    private final SearchResponse.Aggregations aggregations;

    @JsonCreator
    public SearchResponseResource(@JsonProperty("data") PagedResources<Resource<RestaurantDto>> data,
                                  @JsonProperty("aggregations")SearchResponse.Aggregations aggregations) {
        this.data = data;
        this.aggregations = aggregations;
    }

    public PagedResources<Resource<RestaurantDto>> getData() {
        return data;
    }

    public SearchResponse.Aggregations getAggregations() {
        return aggregations;
    }
}
