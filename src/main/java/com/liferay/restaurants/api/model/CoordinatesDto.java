package com.liferay.restaurants.api.model;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.springframework.data.elasticsearch.core.geo.GeoPoint;

import java.io.Serializable;
import java.util.Objects;

/**
 * @author david.arques
 */
public class CoordinatesDto implements Serializable {

    private double latitude;
    private double longitude;

    @JsonCreator
    public CoordinatesDto(@JsonProperty("latitude") double latitude,
                          @JsonProperty("longitude") double longitude) {
        this.latitude = latitude;
        this.longitude = longitude;
    }

    public static CoordinatesDto fromModel(GeoPoint c){
        return new CoordinatesDto(c.getLat(), c.getLon());
    }

    public double getLatitude() {
        return this.latitude;
    }
    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return this.longitude;
    }
    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CoordinatesDto that = (CoordinatesDto) o;
        return Double.compare(that.latitude, latitude) == 0 &&
                Double.compare(that.longitude, longitude) == 0;
    }

    @Override
    public int hashCode() {

        return Objects.hash(latitude, longitude);
    }

    @Override
    public String toString() {
        return "CoordinatesDto{" +
                "latitude=" + latitude +
                ", longitude=" + longitude +
                '}';
    }
}
