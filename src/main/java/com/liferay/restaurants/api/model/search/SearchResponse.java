package com.liferay.restaurants.api.model.search;

import com.liferay.restaurants.model.domain.restaurant.Restaurant;
import org.springframework.data.domain.Page;

import java.util.HashMap;

/**
 * @author david.arques
 */
public class SearchResponse {

    private final Page<Restaurant> data;
    private final Aggregations aggregations;

    public SearchResponse(Page<Restaurant> data, Aggregations aggregations) {
        this.aggregations = aggregations;
        this.data = data;
    }

    public Page<Restaurant> getData() {
        return data;
    }

    public Aggregations getAggregations() {
        return aggregations;
    }

    public static class Aggregations extends HashMap<String, Aggregation> {}

    public static class Aggregation extends HashMap<String, Long>{}

}
