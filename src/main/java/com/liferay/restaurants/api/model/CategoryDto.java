package com.liferay.restaurants.api.model;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.liferay.restaurants.model.domain.restaurant.Category;
import org.hibernate.validator.constraints.NotEmpty;

import java.io.Serializable;
import java.util.Objects;

/**
 * @author david.arques
 */
public class CategoryDto implements Serializable {

    @NotEmpty
    private String name;
    private String displayName;

    @JsonCreator
    public CategoryDto(@JsonProperty("name") String name,
                       @JsonProperty("displayName") String displayName) {
        this.name = name;
        this.displayName = displayName;
    }

    public static CategoryDto fromModel(Category c){
        return new CategoryDto(c.getName(), c.getDisplayName());
    }

    public String getName() {
        return this.name;
    }
    public void setName(String name) {
        this.name = name;
    }

    public String getDisplayName() {
        return this.displayName;
    }
    public void setDisplayName(String displayName) {
        this.displayName = displayName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        CategoryDto that = (CategoryDto) o;
        return Objects.equals(name, that.name) &&
                Objects.equals(displayName, that.displayName);
    }

    @Override
    public int hashCode() {

        return Objects.hash(name, displayName);
    }

    @Override
    public String toString() {
        return "CategoryDto{" +
                "name='" + name + '\'' +
                ", displayName='" + displayName + '\'' +
                '}';
    }
}
