package com.liferay.restaurants.api.model.search;

import com.liferay.restaurants.model.domain.restaurant.RestaurantIllegalArgumentException;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.util.StringUtils;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

/**
 * @author david.arques
 */
public class SearchRequest {


    private String term; //Search term (e.g. "food", "restaurants")
    private Filter filter;
    private Pageable pageAndSort;
    public static final Map<String, String> SORT_FIELD_ALIAS = sortFieldsAlias(); //price -> internal price.value

    public SearchRequest(String term, Filter filter, Pageable pageAndSort) {
        this.term = term;
        this.filter = filter;
        this.pageAndSort = transformInternalProperties(pageAndSort);
    }

    private Pageable transformInternalProperties(Pageable pageAndSort) {
        if(pageAndSort.getSort() != null){
            return new PageRequest(pageAndSort.getPageNumber(), pageAndSort.getPageSize(),
                    new Sort(StreamSupport.stream(pageAndSort.getSort().spliterator(), false)
                    .map(order -> new Sort.Order(order.getDirection(),
                            SORT_FIELD_ALIAS.containsKey(order.getProperty())
                                    ? SORT_FIELD_ALIAS.get(order.getProperty())
                                    : order.getProperty())).collect(Collectors.toList())));
        }
        return pageAndSort;
    }

    private static Map<String, String> sortFieldsAlias() {
        Map<String, String> map = new HashMap<>();
        map.put("price", "price.value");
        map.put("rating", "rating.value");
        return Collections.unmodifiableMap(map);
    }

    public String getTerm() {
        return term;
    }

    public Filter getFilter() {
        return filter;
    }

    public Pageable getPageAndSort() {
        return pageAndSort;
    }

    public static class Filter {

        private Double latitude; // Latitude of the location you want to search nearby
        private Double longitude; //Longitude of the location you want to search nearby
        private Integer radius; //Search radius in meters

        private String[] categories = new String[0]; //Categories to filter the search results with "bars,french,japanese"
        private int[] price = new int[0]; // Pricing levels to filter the search result with. price (1 -> $, 1 -> $, 2-> $$,...,5)
        private int[] rating = new int[0]; // min_rating (1 -> [0-1], 2 -> [1-2], 3-> [2-3], 4-> [3-4], 5->[4-5])

        private String[] countries = new String[0]; // "US,ES,UK" (ISO 3166)
        private String[] cities = new String[0]; // "Madrid, Barcelona, Murcia"
        private String[] states = new String[0]; // "CA,WA"

        public Filter(String categories, String price, String rating, String countries, String cities, String states,
                      Double latitude, Double longitude, Integer radius) {

            if(radius != null && radius < 0){
                throw new RestaurantIllegalArgumentException("Invalid radius meters " + radius);
            }

            if(StringUtils.hasText(categories)){
                this.categories = Arrays.stream(categories.split(","))
                        .map(String::trim)
                        .toArray(String[]::new);
            }

            if(StringUtils.hasText(price)){
                this.price = Arrays.stream(price.split(","))
                        .map(String::trim)
                        .mapToInt(Integer::parseInt).toArray();
            }

            if(StringUtils.hasText(rating)){
                this.rating = Arrays.stream(rating.split(","))
                        .map(String::trim)
                        .mapToInt(Integer::parseInt).toArray();
            }

            if(StringUtils.hasText(countries)){
                this.countries = Arrays.stream(countries.split(","))
                        .map(String::trim)
                        .toArray(String[]::new);
            }

            if(StringUtils.hasText(cities)){
                this.cities = Arrays.stream(cities.split(","))
                        .map(String::trim)
                        .toArray(String[]::new);
            }

            if(StringUtils.hasText(states)){
                this.states = Arrays.stream(states.split(","))
                        .map(String::trim)
                        .toArray(String[]::new);
            }

            this.latitude = latitude;
            this.longitude = longitude;
            this.radius = radius;
        }

        public Optional<Double> getLatitude() {
            return Optional.ofNullable(latitude);
        }

        public Optional<Double> getLongitude() {
            return Optional.ofNullable(longitude);
        }

        public Optional<Integer> getRadius() {
            return Optional.ofNullable(radius);
        }

        public String[] getCategories() {
            return categories;
        }

        public int[] getPrice() {
            return price;
        }

        public int[] getRating() {
            return rating;
        }

        public String[] getCountries() {
            return countries;
        }

        public String[] getCities() {
            return cities;
        }

        public String[] getStates() {
            return states;
        }

        @Override
        public String toString() {
            return "Filter{" +
                    "latitude=" + latitude +
                    ", longitude=" + longitude +
                    ", radius=" + radius +
                    ", categories=" + Arrays.toString(categories) +
                    ", price=" + Arrays.toString(price) +
                    ", rating=" + Arrays.toString(rating) +
                    ", countries=" + Arrays.toString(countries) +
                    ", cities=" + Arrays.toString(cities) +
                    ", states=" + Arrays.toString(states) +
                    '}';
        }
    }


    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        SearchRequest request = (SearchRequest) o;
        return Objects.equals(term, request.term) &&
                Objects.equals(filter, request.filter) &&
                Objects.equals(pageAndSort, request.pageAndSort);
    }

    @Override
    public int hashCode() {
        return Objects.hash(term, filter, pageAndSort);
    }

    @Override
    public String toString() {
        return "SearchRequest{" +
                "term='" + term + '\'' +
                ", filter=" + filter +
                ", pageAndSort=" + pageAndSort +
                '}';
    }
}
