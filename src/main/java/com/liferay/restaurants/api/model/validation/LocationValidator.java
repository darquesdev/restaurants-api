package com.liferay.restaurants.api.model.validation;

import com.liferay.restaurants.api.model.LocationDto;
import com.liferay.restaurants.application.UsaAddressValidator;
import com.liferay.restaurants.model.domain.restaurant.DomainRegistry;
import org.apache.commons.lang.StringUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;

public class LocationValidator implements ConstraintValidator<Location, LocationDto> {

    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    @Override
    public void initialize(Location constraintAnnotation) {
        logger.info("Initializing LocationValidator...");
    }

    @Override
    public boolean isValid(LocationDto aLocation, ConstraintValidatorContext constraintValidatorContext) {
        logger.info("Validating location {}", aLocation);
        return addressNotEmpty(aLocation)
                && isValidCountryCode(aLocation)
                && (!UsaAddressValidator.isUsa(aLocation.getCountry()) || isUsaAddressValid(aLocation));
    }

    private boolean isUsaAddressValid(LocationDto anAddress) {
        return UsaAddressValidator.isValidUsaPostalCode(anAddress.getPostalCode()) && !UsaAddressValidator.isValidUsaState(anAddress.getState());
    }

    private boolean isValidCountryCode(LocationDto anAddress) {
        return DomainRegistry.isoCountryCodeValidator().isValidIsoCountryCode(anAddress.getCountry());
    }

    private boolean addressNotEmpty(LocationDto aLocation) {
        return aLocation != null && !StringUtils.isEmpty(aLocation.getAddress1())
                && !StringUtils.isEmpty(aLocation.getCountry())
                && !StringUtils.isEmpty(aLocation.getPostalCode())
                && !StringUtils.isEmpty(aLocation.getCity())
                && !StringUtils.isEmpty(aLocation.getState());
    }
}
