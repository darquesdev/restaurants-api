package com.liferay.restaurants.api;

import com.liferay.restaurants.api.model.RestaurantDto;
import com.liferay.restaurants.api.model.RestaurantEditableDto;
import com.liferay.restaurants.api.model.search.SearchRequest;
import com.liferay.restaurants.api.model.search.SearchResponse;
import com.liferay.restaurants.api.model.search.SearchResponseResource;
import com.liferay.restaurants.application.RestaurantsApplicationService;
import com.liferay.restaurants.model.domain.restaurant.Restaurant;
import com.liferay.restaurants.model.domain.restaurant.RestaurantsRuntimeException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.web.HateoasPageableHandlerMethodArgumentResolver;
import org.springframework.data.web.PagedResourcesAssembler;
import org.springframework.hateoas.*;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.util.Assert;
import org.springframework.web.bind.annotation.*;

import javax.annotation.PostConstruct;
import javax.validation.Valid;
import java.net.URI;
import java.util.Optional;
import java.util.function.Function;

import static org.springframework.web.bind.annotation.RequestMethod.DELETE;
import static org.springframework.web.bind.annotation.RequestMethod.GET;
import static org.springframework.web.bind.annotation.RequestMethod.PUT;

@RestController
@RequestMapping("/api/restaurants")
@ExposesResourceFor(RestaurantDto.class)
public class RestaurantsRestController {

    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    private final RestaurantsApplicationService applicationService;
    private final EntityLinks entityLinks;

    @Autowired
    public RestaurantsRestController(RestaurantsApplicationService applicationService, EntityLinks entityLinks) {
        logger.info("Creating RestaurantsRestController...");
        Assert.notNull(applicationService, "Restaurants Application Service is mandatory");
        Assert.notNull(entityLinks, "EntityLinks is mandatory");
        this.applicationService = applicationService;
        this.entityLinks = entityLinks;
    }

    @PostConstruct
    private void init() {
        logger.info("RestaurantsRestController created");
    }

    @RequestMapping(method = GET)
    public HttpEntity<SearchResponseResource> showRestaurants(
            @RequestParam(value = "categories", required = false) String categories,
            @RequestParam(value = "price", required = false) String price,
            @RequestParam(value = "rating", required = false) String rating,
            Pageable pageAndSort) {

        return searchRestaurants(categories, categories, price, rating, null, null, null,
                null, null, null, pageAndSort);
    }

    @RequestMapping(path = "search", method = GET)
    public HttpEntity<SearchResponseResource> searchRestaurants(
            @RequestParam(value = "term", required = false) String term,
            @RequestParam(value = "categories", required = false) String categories,
            @RequestParam(value = "price", required = false) String price,
            @RequestParam(value = "rating", required = false) String rating,
            @RequestParam(value = "countries", required = false) String countries,
            @RequestParam(value = "cities", required = false) String cities,
            @RequestParam(value = "states", required = false) String states,
            @RequestParam(value = "latitude", required = false) Double latitude,
            @RequestParam(value = "longitude", required = false) Double longitude,
            @RequestParam(value = "radius", required = false) Integer radius,
            Pageable pageAndSort) {

        logger.info("searching restaurants with pagination {}, categories {}, price {}, rating {}, countries {}, cities {}, " +
                        "states {}, latitude {}, longitude {}, radius {}", pageAndSort, categories, price, rating, countries,
                cities, states, latitude, longitude, radius);

        SearchRequest request = new SearchRequest(term,
                new SearchRequest.Filter(categories, price, rating, countries, cities, states, latitude, longitude, radius),
                pageAndSort);
        logger.debug("Initial search request {}", request);

        SearchResponse response = applicationService.searchRestaurants(request);

        logger.debug("Search Response {}", response);

        HateoasPageableHandlerMethodArgumentResolver resolver = new HateoasPageableHandlerMethodArgumentResolver();
        PagedResourcesAssembler<RestaurantDto> assembler = new PagedResourcesAssembler<>(resolver, null);

        Page<RestaurantDto> restaurants = response.getData().map(RestaurantDto::fromModel);
        PagedResources<Resource<RestaurantDto>> restaurantsResources = assembler.toResource(restaurants);
        restaurantsResources.getContent().forEach(r -> r.add(entityLinks.linkToSingleResource(RestaurantDto.class, r.getContent().getRestaurantId())));
        SearchResponseResource searchResponseResource = new SearchResponseResource(restaurantsResources, response.getAggregations());
        logger.info("Search successfully performed");
        return new ResponseEntity<>(searchResponseResource, HttpStatus.OK);
    }

    @RequestMapping(path = "{id}", method = GET)
    public ResponseEntity<RestaurantDto> getRestaurant(@PathVariable("id") String id){

        return applicationService.findRestaurant(id)
                .map(r -> ResponseEntity.ok(addSelfLink(RestaurantDto.fromModel(r))))
                .orElse(ResponseEntity.notFound().build());
    }


    private RestaurantDto addSelfLink(RestaurantDto rep) {
        rep.add(entityLinks.linkToSingleResource(RestaurantDto.class, rep.getRestaurantId()));
        return rep;
    }


    @RequestMapping(method = RequestMethod.POST)
    public ResponseEntity<Void> createRestaurant(@Valid @RequestBody RestaurantEditableDto aRestaurant) {
        logger.info("Creating restaurant {}", aRestaurant);

        Optional<Restaurant> newRestaurant = applicationService.createRestaurant(aRestaurant);
        if(!newRestaurant.isPresent()){
            throw new RestaurantsRuntimeException(String.format("Error creating restaurant %s", aRestaurant));
        }

        RestaurantDto resource = RestaurantDto.fromModel(newRestaurant.get());
        resource.add(entityLinks.linkToSingleResource(RestaurantDto.class, newRestaurant.get().getId()));
        if (logger.isInfoEnabled()) {
            logger.info("Restaurant created {}, createdAt {}", newRestaurant.get().getId(), newRestaurant.get().getCreatedAt());
        }

        return ResponseEntity.created(URI.create(resource.getLink(Link.REL_SELF).getHref())).build();
    }

    @RequestMapping(path = "{id}", method = DELETE)
    public ResponseEntity<Void> deleteRestaurant(@PathVariable("id") String aRestaurantId) {
        logger.info("deleting restaurant {}", aRestaurantId);

        if(!applicationService.restaurantExists(aRestaurantId)){
            logger.info("restaurant {} not found", aRestaurantId);
            return ResponseEntity.notFound().build();
        }

        applicationService.deleteRestaurant(aRestaurantId);
        logger.info("restaurant deleted {}", aRestaurantId);
        return ResponseEntity.ok().build();
    }

    @RequestMapping(path = "{id}", method = PUT)
    public ResponseEntity<Void> updateRestaurant(@PathVariable("id") String aRestaurantId,
                                                 @RequestBody @Valid RestaurantEditableDto aRestaurant) {
        logger.info("updating restaurant: {}", aRestaurantId);
        if(!applicationService.restaurantExists(aRestaurantId)){
            logger.info("restaurant {} not found", aRestaurantId);
            return ResponseEntity.notFound().build();
        }

        return applicationService.updateRestaurant(aRestaurantId, aRestaurant)
                .map((Function<Restaurant, ResponseEntity<Void>>) restaurant -> {
                    logger.info("restaurant udpated {}, modifiedAt {}", restaurant.getId(), restaurant.getModifiedAt());
                    return ResponseEntity.ok().build();
                })
                .orElseThrow(() -> new RestaurantsRuntimeException("Error updating restaurant " + aRestaurantId));
    }

}
