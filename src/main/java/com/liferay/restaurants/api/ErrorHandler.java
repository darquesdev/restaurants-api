package com.liferay.restaurants.api;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.liferay.restaurants.model.domain.restaurant.PhoneNumberParsingException;
import com.liferay.restaurants.model.domain.restaurant.RestaurantNotFoundException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import java.util.ArrayList;
import java.util.List;

import static org.springframework.http.HttpStatus.BAD_REQUEST;
import static org.springframework.http.HttpStatus.INTERNAL_SERVER_ERROR;
import static org.springframework.http.HttpStatus.NOT_FOUND;

@Order(Ordered.HIGHEST_PRECEDENCE)
@ControllerAdvice()
public class ErrorHandler {

    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    @ResponseStatus(NOT_FOUND)
    @ResponseBody
    @ExceptionHandler(RestaurantNotFoundException.class)
    public Error restaurantNotFoundException(RestaurantNotFoundException ex) {
        logger.error(ex.getMessage(), ex);
        return new Error(NOT_FOUND.value(), ex.getLocalizedMessage());
    }

    @ResponseStatus(BAD_REQUEST)
    @ResponseBody
    @ExceptionHandler(PhoneNumberParsingException.class)
    public Error phoneNumberParsingException(PhoneNumberParsingException ex) {
        logger.error(ex.getMessage(), ex);
        return new Error(BAD_REQUEST.value(), ex.getLocalizedMessage());
    }

    @ResponseStatus(BAD_REQUEST)
    @ResponseBody
    @ExceptionHandler(MethodArgumentNotValidException.class)
    public Error methodArgumentNotValidException(MethodArgumentNotValidException ex) {
        logger.error(ex.getMessage(), ex);
        BindingResult result = ex.getBindingResult();
        List<org.springframework.validation.FieldError> fieldErrors = result.getFieldErrors();
        return processFieldErrors(fieldErrors);
    }

    @ResponseStatus(BAD_REQUEST)
    @ResponseBody
    @ExceptionHandler(HttpMessageNotReadableException.class)
    public Error httpMessageNotReadableException(HttpMessageNotReadableException ex) {
        logger.error(ex.getMessage(), ex);
        return new Error(BAD_REQUEST.value(), "format error: " + ex.getLocalizedMessage());
    }

    @ResponseStatus(INTERNAL_SERVER_ERROR)
    @ResponseBody
    @ExceptionHandler(Exception.class)
    public Error generalException(Exception ex) {
        logger.error(ex.getMessage(), ex);
        return new Error(INTERNAL_SERVER_ERROR.value(), "internal error: " + ex.getLocalizedMessage());
    }

    private Error processFieldErrors(List<org.springframework.validation.FieldError> fieldErrors) {
        Error error = new Error(BAD_REQUEST.value(), "validation error");
        for (org.springframework.validation.FieldError fieldError : fieldErrors) {
            error.addFieldError(fieldError.getField(), fieldError.getDefaultMessage());
        }
        return error;
    }

    @JsonInclude(JsonInclude.Include.NON_EMPTY)
    static class Error {

        private final int status;
        private final String message;
        private List<FieldError> fieldErrors = new ArrayList<>();

        Error(int status, String message) {
            this.status = status;
            this.message = message;
        }

        public int getStatus() {
            return status;
        }

        public String getMessage() {
            return message;
        }

        void addFieldError(String path, String message) {
            FieldError error = new FieldError(path, message);
            fieldErrors.add(error);
        }

        public List<FieldError> getFieldErrors() {
            return fieldErrors;
        }
    }

    static class FieldError {

        private String field;

        private String message;

        FieldError(String field, String message) {
            this.field = field;
            this.message = message;
        }

        public String getField() {
            return field;
        }

        public String getMessage() {
            return message;
        }
    }
}