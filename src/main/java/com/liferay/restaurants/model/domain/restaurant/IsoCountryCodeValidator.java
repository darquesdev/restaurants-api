package com.liferay.restaurants.model.domain.restaurant;

public interface IsoCountryCodeValidator {

    boolean isValidIsoCountryCode(String anIsoCountryCode);

}
