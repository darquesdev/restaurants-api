package com.liferay.restaurants.model.domain.restaurant;

import com.liferay.restaurants.model.domain.common.ValueObject;

public abstract class ValueObjectBase extends ValueObject{

    @Override
    protected void throwIllegalArgumentException(String aMessage) {
        throw new RestaurantIllegalArgumentException(aMessage);
    }

    @Override
    protected void throwIllegalStateException(String aMessage) {
        throw new RestaurantIllegalStateException(aMessage);
    }
}
