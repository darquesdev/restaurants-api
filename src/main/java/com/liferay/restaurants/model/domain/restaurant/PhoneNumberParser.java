package com.liferay.restaurants.model.domain.restaurant;

public interface PhoneNumberParser {

    /**
     * Parse the provided telephone number
     *
     * @param aPhoneNumber a Phone Number
     * @param aCountryCode a Country Code
     * @return a Parsed PhoneNumber
     * @throws PhoneNumberParsingException indicating the parsing error
     */
    ParsedPhoneNumber parse(String aPhoneNumber, String aCountryCode);

    boolean isValidPhoneNumber(String aPhoneNumber, String aIsoCountryCode);

}
