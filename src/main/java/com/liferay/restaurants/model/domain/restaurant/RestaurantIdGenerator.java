package com.liferay.restaurants.model.domain.restaurant;

public interface RestaurantIdGenerator {

    String generateId();
}
