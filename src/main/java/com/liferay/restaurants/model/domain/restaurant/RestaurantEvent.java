package com.liferay.restaurants.model.domain.restaurant;

import com.liferay.restaurants.model.domain.common.DomainEvent;

import java.time.LocalDateTime;

public abstract class RestaurantEvent implements DomainEvent {

    private final LocalDateTime occurredOn;

    public RestaurantEvent() {
        this.occurredOn = LocalDateTime.now();
    }

    @Override
    public LocalDateTime occurredOn() {
        return occurredOn;
    }

    public abstract Type type();

    public enum Type {
        RESTAURANT_CREATED, RESTAURANT_UPDATED, RESTAURANT_DELETED
    }
}
