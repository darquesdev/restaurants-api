package com.liferay.restaurants.model.domain.restaurant;


import java.util.Objects;

public class RestaurantUpdated extends RestaurantEvent {

    private final String restaurantId;

    public RestaurantUpdated(String restaurantId) {
        super();
        this.restaurantId = restaurantId;
    }

    public String getRestaurantId() {
        return restaurantId;
    }

    @Override
    public Type type() {
        return Type.RESTAURANT_UPDATED;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        RestaurantUpdated that = (RestaurantUpdated) o;
        return Objects.equals(restaurantId, that.restaurantId);
    }

    @Override
    public int hashCode() {

        return Objects.hash(restaurantId);
    }

    @Override
    public String toString() {
        return "RestaurantUpdated{" +
                "restaurantId='" + restaurantId + '\'' +
                '}';
    }
}
