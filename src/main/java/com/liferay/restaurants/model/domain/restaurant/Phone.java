package com.liferay.restaurants.model.domain.restaurant;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.springframework.data.elasticsearch.annotations.Field;
import org.springframework.data.elasticsearch.annotations.FieldIndex;
import org.springframework.data.elasticsearch.annotations.FieldType;

import java.util.Objects;

public class Phone extends ValueObjectBase {

    @Field(index = FieldIndex.not_analyzed, store = true, type = FieldType.String)
    private final String number;

    @Field(index = FieldIndex.not_analyzed, store = true, type = FieldType.String)
    private final String internationalNumber;

    @Field(index = FieldIndex.not_analyzed, store = true, type = FieldType.String)
    private final String isoCountryCode; //ES, US

    @JsonCreator
    public Phone(@JsonProperty("number") String number,
                 @JsonProperty("isoCountryCode") String isoCountryCode) {
        super();
        ParsedPhoneNumber aValidatedPhoneNumber = DomainRegistry.phoneNumberParser().parse(number, isoCountryCode);
        this.internationalNumber = aValidatedPhoneNumber.getFormattedInternationalNumber();
        this.isoCountryCode = aValidatedPhoneNumber.getIsoCountryCode();
        this.number = aValidatedPhoneNumber.getFormattedNationalNumber();
    }

    public String getNumber() {
        return number;
    }

    public String getInternationalNumber() {
        return internationalNumber;
    }

    public String getIsoCountryCode() {
        return isoCountryCode;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Phone phone = (Phone) o;
        return Objects.equals(number, phone.number) &&
                Objects.equals(internationalNumber, phone.internationalNumber) &&
                Objects.equals(isoCountryCode, phone.isoCountryCode);
    }

    @Override
    public int hashCode() {

        return Objects.hash(number, internationalNumber, isoCountryCode);
    }

    @Override
    public String toString() {
        return "Phone{" +
                "number='" + number + '\'' +
                ", internationalNumber='" + internationalNumber + '\'' +
                ", isoCountryCode='" + isoCountryCode + '\'' +
                '}';
    }
}
