package com.liferay.restaurants.model.domain.restaurant;


import java.util.Objects;

public class RestaurantCreated extends RestaurantEvent {

    private final String restaurantId;
    private final String restaurantName;

    public RestaurantCreated(String restaurantId, String restaurantName) {
        super();
        this.restaurantId = restaurantId;
        this.restaurantName = restaurantName;
    }

    public String getRestaurantId() {
        return restaurantId;
    }

    public String getRestaurantName() {
        return restaurantName;
    }

    @Override
    public Type type() {
        return Type.RESTAURANT_CREATED;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        RestaurantCreated that = (RestaurantCreated) o;
        return Objects.equals(restaurantId, that.restaurantId);
    }

    @Override
    public int hashCode() {

        return Objects.hash(restaurantId);
    }

    @Override
    public String toString() {
        return "RestaurantCreated{" +
                "restaurantId='" + restaurantId + '\'' +
                '}';
    }
}
