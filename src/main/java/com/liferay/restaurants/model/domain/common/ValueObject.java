package com.liferay.restaurants.model.domain.common;

/**
 * @author david.arques
 */
public abstract class ValueObject extends AssertionConcern {

    public ValueObject() {
        super();
    }
}
