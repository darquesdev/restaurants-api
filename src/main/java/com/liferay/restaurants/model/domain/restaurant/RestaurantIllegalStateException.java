package com.liferay.restaurants.model.domain.restaurant;

public class RestaurantIllegalStateException extends RestaurantsRuntimeException {

    public RestaurantIllegalStateException(String aMsg) {
        super(aMsg);
    }
}
