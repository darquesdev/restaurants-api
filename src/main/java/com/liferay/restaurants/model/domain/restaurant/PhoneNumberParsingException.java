package com.liferay.restaurants.model.domain.restaurant;

public class PhoneNumberParsingException extends RestaurantsRuntimeException {

    public PhoneNumberParsingException(String message) {
        super(message);
    }

    public PhoneNumberParsingException(String message, Throwable cause) {
        super(message, cause);
    }
}
