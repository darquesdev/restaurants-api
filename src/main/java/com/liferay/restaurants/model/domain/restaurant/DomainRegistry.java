package com.liferay.restaurants.model.domain.restaurant;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.stereotype.Component;

@Component
public class DomainRegistry implements ApplicationContextAware {

    private static final Logger logger = LoggerFactory.getLogger(DomainRegistry.class);

    private static ApplicationContext applicationContext;
    private static PhoneNumberParser phoneNumberParser;
    private static IsoCountryCodeValidator isoCountryCodeValidator;

    public static PhoneNumberParser phoneNumberParser() {
        if (phoneNumberParser != null) {
            return phoneNumberParser;
        }
        phoneNumberParser = (PhoneNumberParser) applicationContext.getBean("phoneNumberParser");
        return phoneNumberParser;
    }

    public static IsoCountryCodeValidator isoCountryCodeValidator() {
        if (isoCountryCodeValidator != null) {
            return isoCountryCodeValidator;
        }
        isoCountryCodeValidator = (IsoCountryCodeValidator) applicationContext.getBean("isoCountryCodeValidator");
        return isoCountryCodeValidator;
    }

    // testing purpose
    public static void replaceServiceImplementations(PhoneNumberParser phoneNumberParser,
                                                     IsoCountryCodeValidator isoCountryCodeValidator) {
        logger.debug("Replacing beans manually...");
        DomainRegistry.phoneNumberParser = phoneNumberParser;
        DomainRegistry.isoCountryCodeValidator = isoCountryCodeValidator;
    }

    @Override
    public synchronized void setApplicationContext(ApplicationContext anApplicationContext) throws BeansException {//NOSONAR

        if (DomainRegistry.applicationContext == null) {
            DomainRegistry.applicationContext = anApplicationContext;
        }
    }

}
