package com.liferay.restaurants.model.domain.restaurant;

public class RestaurantsRuntimeException extends RuntimeException {

    public RestaurantsRuntimeException(String message) {
        super(message);
    }

    public RestaurantsRuntimeException(String message, Throwable cause) {
        super(message, cause);
    }
}
