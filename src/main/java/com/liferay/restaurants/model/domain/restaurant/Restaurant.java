package com.liferay.restaurants.model.domain.restaurant;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.springframework.data.annotation.Id;
import org.springframework.data.elasticsearch.annotations.Document;
import org.springframework.data.elasticsearch.annotations.Field;
import org.springframework.data.elasticsearch.annotations.FieldIndex;
import org.springframework.data.elasticsearch.annotations.FieldType;
import org.springframework.data.elasticsearch.core.geo.GeoPoint;

import java.util.Date;
import java.util.List;
import java.util.Objects;

import static com.liferay.restaurants.model.domain.restaurant.Restaurant.INDEX_NAME;
import static com.liferay.restaurants.model.domain.restaurant.Restaurant.TYPE;

@Document(indexName = INDEX_NAME, type = TYPE)
public class Restaurant extends EntityBase {

    public static final String INDEX_NAME = "liferay";
    public static final String TYPE = "restaurant";

    @Id
    @Field(type = FieldType.String, store = true)
    private final String id;

    @Field(index = FieldIndex.analyzed, store = true, type = FieldType.String)
    private final String name;

    @Field(index = FieldIndex.analyzed, store = true, type = FieldType.String)
    private final String description;

    @Field(type = FieldType.Nested, includeInParent = true)
    private final List<Category> categories;

    @Field(type = FieldType.Object)
    private final Location location;

    @Field(type = FieldType.Object)
    private final Phone phone;

    @Field(type = FieldType.Object)
    private final Price price;

    @Field(type = FieldType.Object)
    private final Rating rating;

    private final GeoPoint coordinates;//NOSONAR

    @Field(type = FieldType.Date)
    private final Date createdAt;//NOSONAR

    @Field(type = FieldType.Date)
    private final Date modifiedAt;//NOSONAR

    @JsonCreator
    public Restaurant(@JsonProperty("id") String id,
                      @JsonProperty("name") String name,
                      @JsonProperty("description") String description,
                      @JsonProperty("categories") List<Category> categories,
                      @JsonProperty("location") Location location,
                      @JsonProperty("coordinates") GeoPoint coordinates,
                      @JsonProperty("phone") Phone phone,
                      @JsonProperty("price") Price price,
                      @JsonProperty("rating") Rating rating,
                      @JsonProperty("createdAt") Date createdAt,
                      @JsonProperty("modifiedAt") Date modifiedAt) {

        super();
        assertArgumentNotEmpty(id, "Restaurant ID is mandatory");
        assertArgumentNotEmpty(name, "Restaurant name is mandatory");
        assertArgumentNotNull(location, "Restaurant location is mandatory");
        this.id = id;
        this.name = name;
        this.description = description;
        this.categories = categories;
        this.location = location;
        this.coordinates = coordinates;
        this.phone = phone;
        this.price = price;
        this.rating = rating;
        this.createdAt = createdAt;
        this.modifiedAt = modifiedAt;
    }

    public static RestaurantBuilder builder(String restaurantId) {
        return new RestaurantBuilder(restaurantId);
    }

    @Override
    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public String getDescription() {
        return description;
    }

    public List<Category> getCategories() {
        return categories;
    }

    public Location getLocation() {
        return location;
    }

    public GeoPoint getCoordinates() {
        return coordinates;
    }

    public Phone getPhone() {
        return phone;
    }

    public Price getPrice() {
        return price;
    }

    public Rating getRating() {
        return rating;
    }

    public Date getCreatedAt() {
        return createdAt;
    }

    public Date getModifiedAt() {
        return modifiedAt;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Restaurant that = (Restaurant) o;
        return Objects.equals(id, that.id) &&
                Objects.equals(name, that.name) &&
                Objects.equals(description, that.description) &&
                Objects.equals(categories, that.categories) &&
                Objects.equals(location, that.location) &&
                Objects.equals(phone, that.phone) &&
                Objects.equals(price, that.price) &&
                Objects.equals(rating, that.rating) &&
                Objects.equals(coordinates, that.coordinates) &&
                Objects.equals(createdAt, that.createdAt) &&
                Objects.equals(modifiedAt, that.modifiedAt);
    }

    @Override
    public int hashCode() {

        return Objects.hash(id, name, description, categories, location, phone, price, rating, coordinates, createdAt, modifiedAt);
    }

    @Override
    public String toString() {
        return "Restaurant{" +
                "id='" + id + '\'' +
                ", name='" + name + '\'' +
                ", description='" + description + '\'' +
                ", categories=" + categories +
                ", location=" + location +
                ", phone=" + phone +
                ", price=" + price +
                ", rating=" + rating +
                ", coordinates=" + coordinates +
                ", createdAt=" + createdAt +
                ", modifiedAt=" + modifiedAt +
                '}';
    }
}
