package com.liferay.restaurants.model.domain.restaurant;

public class ParsedPhoneNumber {

    private final int countryCode;//ES->34,US->1
    private final String isoCountryCode; //ES, US
    private final long nationalNumber;
    private final String internationalNumber;
    private final String formattedNationalNumber;

    public ParsedPhoneNumber(String aIsoCountryCode, int countryCode, long nationalNumber, String formattedNationalNumber, String formattedInternationalNumber) {
        this.countryCode = countryCode;
        this.nationalNumber = nationalNumber;
        this.internationalNumber = formattedInternationalNumber;
        this.isoCountryCode = aIsoCountryCode;
        this.formattedNationalNumber = formattedNationalNumber;
    }

    public String getFormattedInternationalNumber() {
        return internationalNumber;
    }

    public long getNationalNumber() {
        return nationalNumber;
    }

    public int getCountryCode() {
        return countryCode;
    }

    public String getIsoCountryCode() {
        return isoCountryCode;
    }

    public String getFormattedNationalNumber() {
        return formattedNationalNumber;
    }
}
