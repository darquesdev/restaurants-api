package com.liferay.restaurants.model.domain.restaurant;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.liferay.restaurants.model.domain.common.ValueObject;
import org.springframework.data.elasticsearch.annotations.Field;
import org.springframework.data.elasticsearch.annotations.FieldIndex;
import org.springframework.data.elasticsearch.annotations.FieldType;

import java.util.Objects;

/**
 * @author david.arques
 */
public class Category extends ValueObjectBase {

    @Field(index = FieldIndex.not_analyzed, store = true, type = FieldType.String)
    private final String name;

    @Field(index = FieldIndex.analyzed, store = true, type = FieldType.String)
    private final String displayName;

    @JsonCreator
    public Category(@JsonProperty("name") String name,
                    @JsonProperty("displayName") String displayName) {
        super();
        assertArgumentNotEmpty(name, "Category name is mandatory");
        this.name = name;
        this.displayName = displayName;
    }

    public String getName() {
        return name;
    }

    public String getDisplayName() {
        return displayName;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Category category = (Category) o;
        return Objects.equals(name, category.name) &&
                Objects.equals(displayName, category.displayName);
    }

    @Override
    public int hashCode() {

        return Objects.hash(name, displayName);
    }

    @Override
    public String toString() {
        return "Category{" +
                "name='" + name + '\'' +
                ", displayName='" + displayName + '\'' +
                '}';
    }
}
