package com.liferay.restaurants.model.domain.restaurant;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.springframework.data.elasticsearch.annotations.Field;
import org.springframework.data.elasticsearch.annotations.FieldIndex;
import org.springframework.data.elasticsearch.annotations.FieldType;

import java.util.Objects;

public class Price extends ValueObjectBase {

    @Field(index = FieldIndex.not_analyzed, store = true, type = FieldType.Integer)
    private final int value;

    @Field(index = FieldIndex.not_analyzed, store = true, type = FieldType.String)
    private final String formattedValue;

    @JsonCreator
    public Price(@JsonProperty("value") int value) {
        assertArgumentRange(value, 0, 5, "Price value must be between 0 and 5");
        this.value = value;
        this.formattedValue = formatValue(value);
    }

    public int getValue() {
        return value;
    }

    public String getFormattedValue() {
        return formattedValue;
    }

    private String formatValue(int value) {
        switch (value) {
            case 1:
                return "$";
            case 2:
                return "$$";
            case 3:
                return "$$$";
            case 4:
                return "$$$$";
            case 5:
                return "$$$$$";
            default:
                return "";
        }
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Price price = (Price) o;
        return value == price.value &&
                Objects.equals(formattedValue, price.formattedValue);
    }

    @Override
    public int hashCode() {

        return Objects.hash(value, formattedValue);
    }

    @Override
    public String toString() {
        return "Price{" +
                "value=" + value +
                ", formattedValue='" + formattedValue + '\'' +
                '}';
    }
}
