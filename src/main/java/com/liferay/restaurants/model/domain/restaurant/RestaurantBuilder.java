package com.liferay.restaurants.model.domain.restaurant;

import org.springframework.data.elasticsearch.core.geo.GeoPoint;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;

public final class RestaurantBuilder {

    private String id;
    private String name;
    private String description;
    private List<Category> categories = new ArrayList<>();
    private String locationAddress1;
    private String locationAddress2;
    private String locationPostalCode;
    private String locationCountryCode;
    private String city;
    private String state;
    private Date createdAt;
    private Date modifiedAt;
    private String phoneNumber;
    private String phoneIsoCountryCode;
    private double ratingValue;
    private int priceValue;
    private double latitude;
    private double longitude;
    private int reviewCount;

    protected RestaurantBuilder(String restaurantId) {
        this.id = restaurantId;
    }

    public RestaurantBuilder withName(String name) {
        this.name = name;
        return this;
    }

    public RestaurantBuilder withDescription(String description) {
        this.description = description;
        return this;
    }

    public RestaurantBuilder withCategories(Category... categories) {
        if (categories != null) {
            this.categories.addAll(Arrays.asList(categories));
        }
        return this;
    }

    public RestaurantBuilder withLocation(Location location) {
        this.locationCountryCode = location.getCountry().getCode();
        this.locationAddress1 = location.getAddress1();
        this.locationAddress2 = location.getAddress2();
        this.locationPostalCode = location.getPostalCode();
        this.city = location.getCity();
        this.state = location.getState();

        return this;
    }

    public RestaurantBuilder withLocation(String address1, String address2, String postalCode, String countryCode, String city, String state) {
        this.locationCountryCode = countryCode;
        this.locationAddress1 = address1;
        this.locationAddress2 = address2;
        this.locationPostalCode = postalCode;
        this.city = city;
        this.state = state;
        return this;
    }

    public RestaurantBuilder withCoordinates(double latitude, double longitude) {
        this.latitude = latitude;
        this.longitude = longitude;
        return this;
    }

    public RestaurantBuilder withPhone(String number, String isoCountryCode) {
        this.phoneNumber = number;
        this.phoneIsoCountryCode = isoCountryCode;
        return this;
    }

    public RestaurantBuilder withPrice(int priceValue) {
        this.priceValue = priceValue;
        return this;
    }

    public RestaurantBuilder withRating(double ratingValue, int reviewCount) {
        this.ratingValue = ratingValue;
        this.reviewCount = reviewCount;
        return this;
    }

    public RestaurantBuilder createdAt(Date createdAt) {
        this.createdAt = createdAt;
        return this;
    }

    public RestaurantBuilder modifiedAt(Date modifiedAt) {
        this.modifiedAt = modifiedAt;
        return this;
    }

    public Restaurant build() {
        return new Restaurant(this.id, this.name, this.description, this.categories,
                new Location(this.locationAddress1, this.locationAddress2, this.locationPostalCode, new IsoCountry(this.locationCountryCode), this.city, this.state),
                new GeoPoint(this.latitude, this.longitude),
                new Phone(phoneNumber, phoneIsoCountryCode),
                new Price(priceValue),
                new Rating(ratingValue, reviewCount),
                createdAt, modifiedAt);
    }
}
