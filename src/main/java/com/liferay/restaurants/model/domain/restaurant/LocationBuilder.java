package com.liferay.restaurants.model.domain.restaurant;

public final class LocationBuilder {

    private String address1;
    private String address2;
    private String postalCode;
    private IsoCountry country;
    private String city;
    private String state;

    protected LocationBuilder() {
    }

    public LocationBuilder withAddress1(String address1) {
        this.address1 = address1;
        return this;
    }

    public LocationBuilder withAddress2(String address2) {
        this.address2 = address2;
        return this;
    }

    public LocationBuilder withPostalCode(String postalCode) {
        this.postalCode = postalCode;
        return this;
    }

    public LocationBuilder withCountry(String code) {
        this.country = new IsoCountry(code);
        return this;
    }

    public LocationBuilder withCity(String city) {
        this.city = city;
        return this;
    }

    public LocationBuilder withState(String state) {
        this.state = state;
        return this;
    }

    public Location build() {
        return new Location(address1, address2, postalCode, country, city, state);
    }
}
