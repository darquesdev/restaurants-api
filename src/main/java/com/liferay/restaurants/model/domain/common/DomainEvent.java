package com.liferay.restaurants.model.domain.common;

import java.time.LocalDateTime;

/**
 * @author david.arques
 */
public interface DomainEvent {

    LocalDateTime occurredOn();
}
