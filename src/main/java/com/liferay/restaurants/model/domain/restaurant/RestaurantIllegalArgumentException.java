package com.liferay.restaurants.model.domain.restaurant;

public class RestaurantIllegalArgumentException extends RestaurantsRuntimeException {

    public RestaurantIllegalArgumentException(String aMsg) {
        super(aMsg);
    }
}
