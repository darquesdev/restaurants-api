package com.liferay.restaurants.model.domain.restaurant;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.liferay.restaurants.model.domain.common.ValueObject;
import org.springframework.data.elasticsearch.annotations.Field;
import org.springframework.data.elasticsearch.annotations.FieldIndex;
import org.springframework.data.elasticsearch.annotations.FieldType;

import java.util.Locale;
import java.util.Objects;

public class IsoCountry extends ValueObjectBase {

    @Field(index = FieldIndex.not_analyzed, store = true, type = FieldType.String)
    private final String code;

    @Field(index = FieldIndex.analyzed, store = true, type = FieldType.String)
    private final String displayCountry;

    @JsonCreator
    public IsoCountry(@JsonProperty("code") String code) {
        super();
        assertArgumentNotEmpty(code, "Country code must be provided");
        assertArgumentTrue(DomainRegistry.isoCountryCodeValidator().isValidIsoCountryCode(code),
                String.format("Country getCode '%s' invalid", code));
        this.code = code.toUpperCase();
        this.displayCountry = new Locale("", this.code).getDisplayCountry();
    }

    public String getCode() {
        return code;
    }

    public String getDisplayCountry() {
        return displayCountry;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        IsoCountry that = (IsoCountry) o;
        return Objects.equals(code, that.code) &&
                Objects.equals(displayCountry, that.displayCountry);
    }

    @Override
    public int hashCode() {

        return Objects.hash(code, displayCountry);
    }

    @Override
    public String toString() {
        return "IsoCountry{" +
                "code='" + code + '\'' +
                ", displayCountry='" + displayCountry + '\'' +
                '}';
    }
}
