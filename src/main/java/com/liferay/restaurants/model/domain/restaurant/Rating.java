package com.liferay.restaurants.model.domain.restaurant;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import org.springframework.data.elasticsearch.annotations.Field;
import org.springframework.data.elasticsearch.annotations.FieldIndex;
import org.springframework.data.elasticsearch.annotations.FieldType;

import java.util.Objects;

public class Rating extends ValueObjectBase {

    @Field(index = FieldIndex.not_analyzed, store = true, type = FieldType.Double)
    private final double value;

    @Field(index = FieldIndex.not_analyzed, store = true, type = FieldType.Integer)
    private final int range;

    @Field(index = FieldIndex.not_analyzed, store = true, type = FieldType.Integer)
    private final int reviewCount;

    @JsonCreator
    public Rating(@JsonProperty("value") double value,
                  @JsonProperty("reviewCount") int reviewCount) {

        assertArgumentRange(value, 0, 5, "Rating must be between 0 and 5");
        assertArgumentRange(reviewCount, 0, Integer.MAX_VALUE, "Review Count must be greater or equal to 0");
        this.reviewCount = reviewCount;
        this.value = value;
        this.range = createRange(value);
    }

    public double getValue() {
        return value;
    }

    public int getReviewCount() {
        return reviewCount;
    }

    public int getRange() {
        return range;
    }

    private int createRange(double value) {

        if(value <= 1){
            return 0;
        }
        else if(value > 1 && value <=2){
            return 1;
        }
        else if(value > 2 && value <=3){
            return 2;
        }
        else if(value > 3 && value <=4){
            return 3;
        }
        else if(value > 4 && value <=5){
            return 5;
        }

        throw new RestaurantIllegalArgumentException("Rating value invalid " + value);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Rating rating = (Rating) o;
        return Double.compare(rating.value, value) == 0 &&
                reviewCount == rating.reviewCount &&
                Objects.equals(range, rating.range);
    }

    @Override
    public int hashCode() {

        return Objects.hash(value, reviewCount);
    }

    @Override
    public String toString() {
        return "Rating{" +
                "value=" + value +
                ", range='" + range + '\'' +
                ", reviewCount=" + reviewCount +
                '}';
    }
}
