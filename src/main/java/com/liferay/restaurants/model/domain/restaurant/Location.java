package com.liferay.restaurants.model.domain.restaurant;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.liferay.restaurants.application.UsaAddressValidator;
import com.liferay.restaurants.model.domain.common.ValueObject;
import org.springframework.data.elasticsearch.annotations.Field;
import org.springframework.data.elasticsearch.annotations.FieldIndex;
import org.springframework.data.elasticsearch.annotations.FieldType;

import java.util.Objects;

public class Location extends ValueObjectBase {

    @Field(index = FieldIndex.analyzed, store = true, type = FieldType.String)
    private final String address1;

    @Field(index = FieldIndex.analyzed, store = true, type = FieldType.String)
    private final String address2;

    @Field(index = FieldIndex.not_analyzed, store = true, type = FieldType.String)
    private final String postalCode;

    @Field(type = FieldType.Object)
    private final IsoCountry country;

    @Field(index = FieldIndex.not_analyzed, store = true, type = FieldType.String)
    private final String city;

    @Field(index = FieldIndex.not_analyzed, store = true, type = FieldType.String)
    private final String state;

    @JsonCreator
    public Location(@JsonProperty("address1") String address1,
                    @JsonProperty("address2") String address2,
                    @JsonProperty("postalCode") String postalCode,
                    @JsonProperty("country") IsoCountry country,
                    @JsonProperty("city") String city,
                    @JsonProperty("state") String state) {
        super();

        this.assertArgumentNotEmpty(address1, "Address must be provided");
        this.assertArgumentNotEmpty(city, "City must be provided");
        this.assertArgumentNotNull(country, "Country Code must be provided");
        this.assertArgumentNotNull(state, "State must be provided");
        if (UsaAddressValidator.isUsa(country)) {
            checkUsAddress(postalCode, city, state);
        }

        this.country = country;
        this.address1 = address1;
        this.address2 = address2;
        this.postalCode = postalCode;
        this.city = city;
        this.state = state;
    }

    public static LocationBuilder builder() {
        return new LocationBuilder();
    }

    //TODO validate city, street..
    private void checkUsAddress(String postalCode, String city, String state) {//NOSONAR
        if (!UsaAddressValidator.isValidUsaPostalCode(postalCode)) {
            this.throwIllegalArgumentException(String.format("USA Postal Code Invalid %s", postalCode));
        }
        if (UsaAddressValidator.isValidUsaState(state)) {
            this.throwIllegalArgumentException(String.format("USA State invalid %s", state));
        }
    }

    public String getAddress1() {
        return address1;
    }

    public String getAddress2() {
        return address2;
    }

    public String getPostalCode() {
        return postalCode;
    }

    public IsoCountry getCountry() {
        return country;
    }

    public String getCity() {
        return city;
    }

    public String getState() {
        return state;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Location location = (Location) o;
        return Objects.equals(address1, location.address1) &&
                Objects.equals(address2, location.address2) &&
                Objects.equals(postalCode, location.postalCode) &&
                Objects.equals(country, location.country) &&
                Objects.equals(city, location.city) &&
                Objects.equals(state, location.state);
    }

    @Override
    public int hashCode() {

        return Objects.hash(address1, address2, postalCode, country, city, state);
    }

    @Override
    public String toString() {
        return "Location{" +
                "address1='" + address1 + '\'' +
                ", address2='" + address2 + '\'' +
                ", postalCode='" + postalCode + '\'' +
                ", country=" + country +
                ", city='" + city + '\'' +
                ", state='" + state + '\'' +
                '}';
    }
}
