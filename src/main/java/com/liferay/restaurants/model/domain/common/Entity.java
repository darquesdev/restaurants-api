package com.liferay.restaurants.model.domain.common;

/**
 * @author david.arques
 */
public abstract class Entity extends AssertionConcern {

    private static final long serialVersionUID = 1L;

    public Entity() {
        super();
    }

    protected abstract String getId();
}
