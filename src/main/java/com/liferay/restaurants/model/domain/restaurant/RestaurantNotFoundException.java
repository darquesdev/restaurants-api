package com.liferay.restaurants.model.domain.restaurant;

public class RestaurantNotFoundException extends RestaurantsRuntimeException {

    public RestaurantNotFoundException(String aRestaurantId) {
        super(String.format("Restaurant with id %s not found", aRestaurantId));
    }
}
