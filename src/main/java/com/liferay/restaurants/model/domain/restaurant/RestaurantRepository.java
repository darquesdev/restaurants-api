package com.liferay.restaurants.model.domain.restaurant;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;

public interface RestaurantRepository {

    Restaurant findOne(String id);

    boolean exists(String id);

    Restaurant save(Restaurant aRestaurant);

    void delete(String aRestaurantId);

    Page<Restaurant> findAll(Pageable pageable);

}
