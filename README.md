# Liferay Backend Engineer Exercise

This is my solution to the Coding Challenge of Liferay. Basically this task involves 
building a REST API using a JVM language to manage a restaurants listing.  

#### Assumptions:

Creating a `restaurant`: `name`, `location`, `phone` must not be empty. With:

- `location`: `address1`, `city`, `postalCode` and `state` must not empty. The location
should be, at least, a valid USA address
- `phone`: must be a valid phone
- `rating`: between 1 and 5
- `price`: between 1 and 5
- `category`: `name` must not empty
   
Example of a well-formed JSON to create a restaurant: 
```javascript
{
  "name": "YOKALOKA",
  "categories": [
    {
      "name": "japanese",
      "displayName": "Japanese"
    },
    {
      "name": "sushi",
      "displayName": "Sushi Bars"
    }
  ],
  "coordinates": {
    "latitude": 40.411446,
    "longitude": -3.698794
  },
  "description": "Punto de encuentro de los amantes del mundo gastron\u00F3mico y cultural japon\u00E9s",
  "location": {
    "address1": "Calle Santa Isabel, 5, Planta Baja",
    "address2": "Mercado de Antón Martín",
    "city": "Madrid",
    "postalCode": "28012",
    "country": "ES",
    "state": "M"
  },
  "phone": {
    "countryCode": "ES",
    "number": "610 60 27 22"
  },
  "price": 3,
  "rating": 4.5,
  "reviewCount": 325
}
```    

Searching restaurants:

-  `radius` parameter represents meters 

### Solution

#### Technological Stack:
- Java 8 and Groovy
- Spring Boot with Gradle
- Spring Testing and Spock
- Swagger with Springfox


#### Design overview:

I've followed some tactical tools of Domain-Driven Design, Clean Code and SOLID, trying 
to give maximum value to the domain. In this way I've tried to make the core of the application as 
agnostic as possible to any technology and totally reusable. 

In a real project it might be not necessary to take this separation to the extreme and it 
would be better to follow a more pragmatic way. I've done it this way for testing purposes.

#### Use of the application:

Test execution
```bash
./gradlew cleanTest test
```

Running the application
```bash
./gradlew bootRun
```

Execution through the jar and more [here](https://docs.spring.io/spring-boot/docs/current/reference/html/using-boot-running-your-application.html)

Access to the doc API rest: <http://localhost:8080/swagger-ui.html>

### Features

Rest Endpoints

1. GET `/api/restaurants`: Get list of restaurants
2. GET `/api/restaurants/{id}`: Get a restaurant
3. POST `/api/restaurants`: Create a restaurant
4. PUT  `/api/restaurants/{id}`: Update a resturant
5. DELETE `/api/restaurants/{id}`: Delete a restaurant

Point `1` meets the requirements:

- Support for pagination: `?page={page}&size={size}`
    - Example: `http://localhost:8080/api/restaurants?page=0&size=20`

- Support for sorting by multiple fields: `?sort={sort1},{dir1}&sort={sort2},{dir2}`
    - Fields: `price`, `rating`, `rating.reviewCount`, etc
    - Example: `http://localhost:8080/api/restaurants?sort=price,asc&sort=rating,desc`
    
- Support for filtering by multiple fields: `?{filter1}={commaSeparatedValues1}&{filter2}={commaSeparatedValues2}`
    - Filters: `categories`, `price`, `countries`, `cities` and `states`
    - Example: `http://localhost:8080/api/restaurants?categories=indian&price=1,2`

*Note*: `RestaurantsApiIntTest` has a bunch of examples
    
### Additional features:

I've developed several extra functionality. The most important ones are: Fulltext search 
with geo-location and aggregates

- GET `/api/restaurants/search`: Search restaurants

    - Fulltext search: `search?term={term}`
    - Geolocation search: `/search?latitude={latitude}&longitude={longitude}&radius={radius}`
    - Aggregates: At the bottom of every list response
    
Example of aggregate

```javasctipt
{
    "data": {...},
    "aggregations": {
        "price": {
            "1": 2,
            "2": 2,
            "3": 2,
            "4": 5
        },
        "rating": {
            "3": 7,
            "5": 4
        },
        "location": {
            "Madrid": 4,
            "Murcia": 5,
            "Brooklyn": 2
        },
        "categories": {
            "mediterranean": 5,
            "traditional": 2,
            "pizza": 5,
            "japanese": 4,
            "asianfusion": 2,
            "tapas": 2,
            "sushi": 2,
            "italian": 3,
            "tabern": 2,
            "signature_cuisine": 2
        },
        "location.country": {
            "ES": 9,
            "US": 2
        }
    }
}
```

##### Another extra functionalities:

- Several `javax.validations` used in the Rest API
- Several exceptions with customized responses to the client
- Synchronous and asynchronous Event Listener implementations
- Added events to inform that a restaurant has been created/updated/deleted



### Testing

We could say that in this application there are thwo types of tests. Purely integration test 
and a mixture of unit and integration. I've followed TDD most of the time.

- Integration/aceptance testing in `RestaurantsApiIntTest`. It starts the whole 
application. I've used Spock (see below) 
- Mix (actually integration tests) in `RestaurantsApiCrudTest`. It tests 
the input and output of the rest controller. They start just a slice of the Spring Boot App 

The acceptance tests are done following BDD and Data Driven Testing with 
the [Spock](http://spockframework.org/) framework. So you can see directly 
that the requirements are complete.

For example, geo-location search functionality:

```groovy
    @Unroll
    def "The list of restaurants has support for searching by geolocation"(double latitude, double longitude, int radius, int total) {

        expect:
        def actualTotalElements = restTemplate.exchange("/api/restaurants/search?latitude={latitude}&longitude={longitude}&radius={radius}",
                HttpMethod.GET, null, SearchResponseResource, latitude, longitude, radius).getBody().getData().getMetadata().totalElements
        actualTotalElements == total

        where:
        latitude   | longitude  | radius | total
        40.411446D | -3.698794D | 1      | 1    // YOKALOKA (Lavapiés, Madrid)
        40.411446D | -3.698794D | 1000   | 2    // YOKALOKA & Yakitoro (Gran Vía, Madrid)
        40.411446D | -3.698794D | 350000 | 3    // YOKALOKA & Yakitoro & La Parranda (Murcia)
        40.411446D | -3.698794D | 6000000| 4    // YOKALOKA & Yakitoro & La Parranda & Best Pizza (Brooklyn)
    }
```

### Additional Questions:

#### Security

To provide security to an API Rest there are different alternatives. For simplicity I would talk
about two of them: oAuth and API Key

Taking into account the characteristics of the application:
- The information is not very sensitive 
- No need for authorization

I'd choose the simplest solution: API Key. In this way, each user of the application will have a unique 
token that we will manage.

The authentication of the client could be done in our application or in previous layers: Gateway, API Manager, etc.

#### Scalability

I'd prepare the application to scale horizontally, so that que can multiply the number of applications. 
To do this, I'd try not to persist any type of user information (stateless).

Instances of the application could be executed in own services or in the Cloud. Applications should be distributed 
with docker images, to boost the redeployment process.

Elasticsearch scales very well. Depending on the load of the application, it would be necessary to create new nodes.

In front of all we need a load balancer. This load balancer could also make as a Gateway if we want to 
standardize the communication of our services to the exterior.

On the other hand, the same Gateway could also be responsible for managing user authentication. Although, we 
should check if it can create bottlenecks.

Finally, it would be very useful to have some cache service, such as redis, to cache the most 
used information.
