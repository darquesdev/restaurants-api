# Liferay Backend Engineer Exercise

## The Restaurants API

### Introduction

The goal of this exercise is to build a well designed HTTP API about Restaurants that can be consumed by several clients (Javascript, iOS, Android...).
The deliverable of this exercise should include code and instructions to build it as well as the documentation or diagrams considered necessary to present the proposed design.

### Functional Requirements

- Clients can obtain a list of restaurants with information about them (name, type of food, location, ratings... etc)
    - Support for pagination
    - Support for sorting by certain fields
    - Support for filtering by certain fields
- Clients can add/delete/update new restaurants
- Decide an additional feature that according to you will add value to the API. You may implement this feature by integrating with an external service (e.g. geolocation, tagging, images, ratings... etc)

Feel free to adapt these functional requirements if you think it is necessary to achieve a better result. Additional features or improvements will be highly appreciated. You will be allowed to present any changes or additional developments you decide to make.

### Technical Requirements

- The API will be consumed using HTTP
- The backend must be written in Java (feel free to use Spring Boot or any other framework)
- Elasticsearch will be used to persist data
- Calls to external services should not increase the response time of the API
- Tests will be highly valued
- Any third-party library can be used to help you achieve the best result you can

### Additional Questions:

#### Security

Provide a high level description of how you would make this API secure.

#### Scalability

Provide a high level description of how you would accomplish a good scalability and high availability of the system.

Note: Answers should be sent in a zip file by email. You MUST NOT make any information about the exercise publicly available. You MUST NOT publish the code you create on repositories which are publicly available.
